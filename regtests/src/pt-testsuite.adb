-- --------------------------------------------------------------------
--  pt-testsuite -- Printer Toolkit testsuite
--  Written by Stephane Carrez (Stephane.Carrez@gmail.com)
--  SPDX-License-Identifier: Apache-2.0
-----------------------------------------------------------------------

with PT.Examples;
package body PT.Testsuite is

   Tests : aliased Util.Tests.Test_Suite;

   function Suite return Util.Tests.Access_Test_Suite is
   begin
      --  Executed in reverse order
      PT.Examples.Add_Tests (Tests'Access);
      return Tests'Access;
   end Suite;

end PT.Testsuite;
