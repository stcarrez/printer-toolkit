-----------------------------------------------------------------------
--  pt-harness -- Unit tests
--  Written by Stephane Carrez (Stephane.Carrez@gmail.com)
--  SPDX-License-Identifier: Apache-2.0
-----------------------------------------------------------------------

with Util.Tests;
with PT.Testsuite;

procedure PT.Harness is

   procedure Harness is new Util.Tests.Harness (PT.Testsuite.Suite);

begin
   Harness ("pt-tests.xml");
end PT.Harness;
