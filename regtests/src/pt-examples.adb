-- --------------------------------------------------------------------
--  pt-examples -- Tests for examples
--  Written by Stephane Carrez (Stephane.Carrez@gmail.com)
--  SPDX-License-Identifier: Apache-2.0
-----------------------------------------------------------------------
with Util.Test_Caller;
package body PT.Examples is

   package Caller is new Util.Test_Caller (Test, "PT.Examples");

   function Example_Path return String is ("./examples/bin/");

   procedure Add_Tests (Suite : in Util.Tests.Access_Test_Suite) is
   begin
      Caller.Add_Test (Suite, "Test Examples.Multi",
                       Test_Multi'Access);
      Caller.Add_Test (Suite, "Test Examples.Multi_SVG",
                       Test_Multi_SVG'Access);
      Caller.Add_Test (Suite, "Test Examples.Multi_Hbar",
                       Test_Multi_Hbar'Access);
      Caller.Add_Test (Suite, "Test Examples.Simple_Bar",
                       Test_Simple_Bar'Access);
      Caller.Add_Test (Suite, "Test Examples.Progress",
                       Test_Progress'Access);
      Caller.Add_Test (Suite, "Test Examples.Colors",
                       Test_Colors'Access);
      Caller.Add_Test (Suite, "Test Examples.Plot_Perf",
                       Test_Plot_Perf'Access);
   end Add_Tests;

   --  ------------------------------
   --  Test launching spdx-tool and checking its usage.
   --  ------------------------------
   procedure Test_Multi (T : in out Test) is
      Path : constant String := Util.Tests.Get_Test_Path ("multi.txt");
      Inp  : constant String := Util.Tests.Get_Path ("Makefile");
      Result : UString;
   begin
      T.Execute (Example_Path & "multi", Inp, Path, Result, 0);
      Util.Tests.Assert_Equal_Files
        (T       => T,
         Expect  => Util.Tests.Get_Path ("regtests/expect/multi.txt"),
         Test    => Path,
         Message => "Invalid output for multi");
   end Test_Multi;

   procedure Test_Multi_SVG (T : in out Test) is
      Path : constant String := Util.Tests.Get_Test_Path ("multi_svg.txt");
      Inp  : constant String := Util.Tests.Get_Path ("Makefile");
      Result : UString;
   begin
      T.Execute (Example_Path & "multi_svg", Inp, Path, Result, 0);
      Util.Tests.Assert_Equal_Files
        (T       => T,
         Expect  => Util.Tests.Get_Path ("regtests/expect/multi_svg.txt"),
         Test    => Path,
         Message => "Invalid output for multi_svg");
   end Test_Multi_SVG;

   procedure Test_Multi_Hbar (T : in out Test) is
      Path : constant String := Util.Tests.Get_Test_Path ("multi-hbar.txt");
      Inp  : constant String := Util.Tests.Get_Path ("Makefile");
      Result : UString;
   begin
      T.Execute (Example_Path & "multi_hbar", Inp, Path, Result, 0);
      Util.Tests.Assert_Equal_Files
        (T       => T,
         Expect  => Util.Tests.Get_Path ("regtests/expect/multi-hbar.txt"),
         Test    => Path,
         Message => "Invalid output for multi_hbar");
   end Test_Multi_Hbar;

   procedure Test_Simple_Bar (T : in out Test) is
      Path : constant String := Util.Tests.Get_Test_Path ("simple-bar.txt");
      Inp  : constant String := Util.Tests.Get_Path ("Makefile");
      Result : UString;
   begin
      T.Execute (Example_Path & "simple_bar", Inp, Path, Result, 0);
      Util.Tests.Assert_Equal_Files
        (T       => T,
         Expect  => Util.Tests.Get_Path ("regtests/expect/simple-bar.txt"),
         Test    => Path,
         Message => "Invalid output for simple_bar");
   end Test_Simple_Bar;

   procedure Test_Progress (T : in out Test) is
      Path : constant String := Util.Tests.Get_Test_Path ("progress.txt");
      Inp  : constant String := Util.Tests.Get_Path ("Makefile");
      Result : UString;
   begin
      T.Execute (Example_Path & "progress first:0:100 second:100:0 middle:10:10",
                 Inp, Path, Result, 0);
      Util.Tests.Assert_Equal_Files
        (T       => T,
         Expect  => Util.Tests.Get_Path ("regtests/expect/progress.txt"),
         Test    => Path,
         Message => "Invalid output for progress");
   end Test_Progress;

   procedure Test_Colors (T : in out Test) is
      Path : constant String := Util.Tests.Get_Test_Path ("colors.txt");
      Inp  : constant String := Util.Tests.Get_Path ("Makefile");
      Result : UString;
   begin
      T.Execute (Example_Path & "colors",
                 Inp, Path, Result, 0);
      Util.Tests.Assert_Equal_Files
        (T       => T,
         Expect  => Util.Tests.Get_Path ("regtests/expect/colors.txt"),
         Test    => Path,
         Message => "Invalid output for colors");
   end Test_Colors;

   procedure Test_Plot_Perf (T : in out Test) is
      Path : constant String := Util.Tests.Get_Test_Path ("plot_perf.txt");
      Inp  : constant String := Util.Tests.Get_Path ("regtests/files/are-aunit.xml");
      Result : UString;
   begin
      T.Execute (Example_Path & "plot_perf " & Inp,
                 Inp, Path, Result, 0);
      Util.Tests.Assert_Equal_Files
        (T       => T,
         Expect  => Util.Tests.Get_Path ("regtests/expect/plot_perf.txt"),
         Test    => Path,
         Message => "Invalid output for colors");
   end Test_Plot_Perf;

end PT.Examples;
