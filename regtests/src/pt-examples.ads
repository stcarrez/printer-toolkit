-- --------------------------------------------------------------------
--  pt-examples -- Tests for examples
--  Written by Stephane Carrez (Stephane.Carrez@gmail.com)
--  SPDX-License-Identifier: Apache-2.0
-----------------------------------------------------------------------

with Util.Tests;
package PT.Examples is

   procedure Add_Tests (Suite : in Util.Tests.Access_Test_Suite);

   type Test is new Util.Tests.Test with null record;

   --  Test multi example.
   procedure Test_Multi (T : in out Test);
   procedure Test_Multi_SVG (T : in out Test);
   procedure Test_Multi_Hbar (T : in out Test);
   procedure Test_Simple_Bar (T : in out Test);
   procedure Test_Progress (T : in out Test);
   procedure Test_Colors (T : in out Test);
   procedure Test_Plot_Perf (T : in out Test);

end PT.Examples;
