-----------------------------------------------------------------------
--  pt-drivers-texts -- Printer driver
--  Copyright (C) 2021, 2023, 2024 Stephane Carrez
--  Written by Stephane Carrez (Stephane.Carrez@gmail.com)
--  SPDX-License-Identifier: Apache-2.0
-----------------------------------------------------------------------
with Ada.Unchecked_Deallocation;

with Util.Strings;
package body PT.Drivers.SVG is

   function To_String (Area : in Rect_Type) return String
   is (Util.Strings.Image (Natural (Area.X))
       & Y_Type'Image (Area.Y) & X_Type'Image (Area.X + Area.W)
         & Y_Type'Image (Area.Y + Area.H));

   procedure Create (Printer : in out Printer_Type;
                     Area    : in Rect_Type;
                     Output  : in Util.Streams.Texts.Print_Stream_Access) is
   begin
      Printer.SVG_Driver := new Driver_Type;
      Printer.SVG_Driver.Printer := Printer'Unchecked_Access;
      Printer.SVG_Driver.Area := Area;
      Printer.SVG_Driver.Clear;
      Printer.Driver := Printer.SVG_Driver.all'Access;
      Printer.SVG_Driver.Stream.Initialize (Output => Output);
      Printer.SVG_Driver.Stream.Set_Indentation (2);
      Printer.SVG_Driver.Stream.Start_Entity ("svg");
      Printer.SVG_Driver.Stream.Write_Attribute ("version", "1.1");
      Printer.SVG_Driver.Stream.Write_Attribute ("xmlns", "http://www.w3.org/2000/svg");
      Printer.SVG_Driver.Stream.Write_Attribute ("viewBox", To_String (Area));
   end Create;

   --  ------------------------------
   --  Set the CSS style to be used within the SVG generated images.
   --  ------------------------------
   procedure CSS_Style (Printer : in out Printer_Type;
                        Style   : in String) is
   begin
      Ada.Strings.Unbounded.Append (Printer.SVG_Driver.CSS, Style);
   end CSS_Style;

   overriding
   procedure Initialize (Driver : in out Printer_Type) is
   begin
      null;
   end Initialize;

   overriding
   procedure Finalize (Driver : in out Printer_Type) is
      procedure Free is
         new Ada.Unchecked_Deallocation (Object => Driver_Type'Class,
                                         Name   => Driver_Access);
   begin
      Free (Driver.SVG_Driver);
   end Finalize;

   --  Release the style when it is no longer used.
   procedure Delete_Style (Printer : in out Printer_Type;
                           Style   : in out Style_Type) is
   begin
      null;
   end Delete_Style;

   --  ------------------------------
   --  Set the character fill style.
   --  ------------------------------
   procedure Set_Fill (Printer : in out Printer_Type;
                       Style   : in Style_Type;
                       Fill    : in Wide_Wide_Character) is
   begin
      Printer.SVG_Driver.Styles (Style.Index).Fill := Fill;
   end Set_Fill;

   --  ------------------------------
   --  Set to use the given background color when drawing with this style.
   --  ------------------------------
   procedure Set_Background (Printer : in out Printer_Type;
                             Style   : in Style_Type;
                             Color   : in Color_Type) is
   begin
      Printer.SVG_Driver.Styles (Style.Index).Background := Color;
   end Set_Background;

   --  ------------------------------
   --  Get the drawing area coordinates.
   --  ------------------------------
   overriding
   function Get_Area (Driver : in Driver_Type) return Rect_Type is
   begin
      return Driver.Area;
   end Get_Area;

   --  ------------------------------
   --  Get the current cliping area coordinates.
   --  ------------------------------
   overriding
   function Get_Clip (Driver : in Driver_Type) return Rect_Type is
   begin
      return Driver.Clip;
   end Get_Clip;

   --  ------------------------------
   --  Get the dimension of the text when it is written with the given style.
   --  ------------------------------
   overriding
   function Get_Dimension (Driver : in Driver_Type;
                           Text   : in String;
                           Style  : in Style_Type) return Dimension_Type is
      Result : Dimension_Type;
   begin
      Result.H := Driver.Line_Size;
      Result.W := Text'Length * Driver.Char_Size;
      return Result;
   end Get_Dimension;

   overriding
   function Get_Wide_Dimension (Driver : in Driver_Type;
                                Text   : in Wide_Wide_String;
                                Style  : in Style_Type) return Dimension_Type is
      Result : Dimension_Type;
   begin
      Result.H := Driver.Line_Size;
      Result.W := Text'Length * Driver.Char_Size;
      return Result;
   end Get_Wide_Dimension;

   overriding
   function Get_Dimension (Driver   : in Driver_Type;
                           Percent  : in Percent_Type;
                           On_Width : in W_Type := 0) return Dimension_Type is
      Result : Dimension_Type;
   begin
      Result.H := Driver.Line_Size;
      Result.W := W_Type (Float (On_Width) * Float (Percent) / 100.0);
      if On_Width = 0 or else On_Width > Driver.Area.W then
         Result.W := W_Type (Float (Driver.Area.W) * Float (Percent) / 100.0);
      else
         Result.W := W_Type (Float (On_Width) * Float (Percent) / 100.0);
      end if;
      return Result;
   end Get_Dimension;

   --  ------------------------------
   --  Create a new style that can be configured, referenced and used.
   --  ------------------------------
   overriding
   procedure Create_Style (Driver : in out Driver_Type;
                           Style  : out Style_Type) is
   begin
      for I in Driver.Styles'Range loop
         if not Driver.Styles (I).Allocated then
            Driver.Styles (I).Allocated := True;
            Style := (Index => I);
            return;
         end if;
      end loop;
   end Create_Style;

   --  ------------------------------
   --  Copy the style definition from one style to another.
   --  ------------------------------
   overriding
   procedure Copy_Style (Driver : in out Driver_Type;
                         From   : in Style_Type;
                         To     : in Style_Type) is
   begin
      Driver.Styles (To.Index) := Driver.Styles (From.Index);
   end Copy_Style;

   --  ------------------------------
   --  Set to use the given color when drawing with this style.
   --  ------------------------------
   overriding
   procedure Set_Color (Driver : in out Driver_Type;
                        Style  : in Style_Type;
                        Color  : in Color_Type) is
   begin
      Driver.Styles (Style.Index).Color := Color;
   end Set_Color;

   --  ------------------------------
   --  Set the style justification.
   --  ------------------------------
   overriding
   procedure Set_Justify (Driver   : in out Driver_Type;
                          Style    : in Style_Type;
                          Justify  : in Justify_Type) is
   begin
      Driver.Styles (Style.Index).Justify := Justify;
   end Set_Justify;

   --  ------------------------------
   --  Set the font style.
   --  ------------------------------
   overriding
   procedure Set_Font (Driver   : in out Driver_Type;
                       Style    : in Style_Type;
                       Font     : in Font_Type) is
   begin
      Driver.Styles (Style.Index).Font := Font;
   end Set_Font;

   --  ------------------------------
   --  Set the current cliping area.
   --  ------------------------------
   overriding
   procedure Clip (Driver : in out Driver_Type;
                   Rect   : in Rect_Type) is
   begin
      Driver.Clip := Rect;
   end Clip;

   --  Fill the rectangle by using the given type.
   --  The rectangle is clipped if it intersects the cliping area.
   overriding
   procedure Fill (Driver : in out Driver_Type;
                   Rect   : in Rect_Type;
                   Style  : in Style_Type) is
      R    : constant Rect_Type := Rect & Driver.Clip;
   begin
      Driver.Stream.Start_Entity ("rect");
      Driver.Stream.Write_Attribute ("x", Driver.To_String (R.X));
      Driver.Stream.Write_Attribute ("y", Driver.To_String (R.Y));
      Driver.Stream.Write_Attribute ("width", Driver.Width_To_String (R.W));
      Driver.Stream.Write_Attribute ("height", Driver.Height_To_String (R.H));
      Driver.Stream.Write_Attribute ("style", Driver.To_String (Style));
      Driver.Stream.End_Entity ("rect");
   end Fill;

   --  Draw a line starting from (X1, Y1) to (X2, Y2) using the given style.
   --  The line is clipped if it intersects the cliping area.
   overriding
   procedure Draw (Driver : in out Driver_Type;
                   X1     : in X_Type;
                   Y1     : in Y_Type;
                   X2     : in X_Type;
                   Y2     : in Y_Type;
                   Rect   : in Rect_Type;
                   Style  : in Style_Type) is
   begin
      null;
   end Draw;

   function To_String (Driver : in Driver_Type;
                       Style  : in Style_Type) return String is
      Info : constant Style_Info_Type := Driver.Styles (Style.Index);
   begin
      return "opacity:1;fill:#" & PT.Colors.To_Hex (Info.Color);
   end To_String;

   function To_String (Driver : in Driver_Type;
                       X      : in X_Type) return String is
   begin
      return Util.Strings.Image (Natural (X));
   end To_String;

   function Width_To_String (Driver : in Driver_Type;
                             W      : in W_Type) return String is
   begin
      return Util.Strings.Image (Natural (W));
   end Width_To_String;

   function Height_To_String (Driver : in Driver_Type;
                              H      : in H_Type) return String is
   begin
      return Util.Strings.Image (Natural (H));
   end Height_To_String;

   function To_String (Driver : in Driver_Type;
                       Y      : in Y_Type) return String is
   begin
      return Util.Strings.Image (Natural (Driver.Y_Offset + Y));
   end To_String;

   --  Write the text starting at the top left X, Y corner and using the
   --  given style.  The text is clipped if it intersects the cliping area.
   --  An UTF-8 sequence must be converted to a Wide_Wide_String and
   --  the Put_Wide method must be used.
   overriding
   procedure Put (Driver  : in out Driver_Type;
                  X       : in X_Type;
                  Y       : in Y_Type;
                  Text    : in String;
                  Style   : in Style_Type;
                  Justify : in Justify_Type := J_DEFAULT) is
   begin
      Driver.Stream.Start_Entity ("text");
      Driver.Stream.Write_Attribute ("style", "font-size:14px");
      Driver.Stream.Write_Attribute ("x", Driver.To_String (X));
      Driver.Stream.Write_Attribute ("y", Driver.To_String (Y + Driver.Line_Size));
      --  Driver.Stream.Start_Entity ("tspan");
      Driver.Stream.Write_String (Text);
      --  Driver.Stream.End_Entity ("tspan");
      Driver.Stream.End_Entity ("text");
   end Put;

   overriding
   procedure Put_Wide (Driver  : in out Driver_Type;
                       X       : in X_Type;
                       Y       : in Y_Type;
                       Text    : in Wide_Wide_String;
                       Style   : in Style_Type;
                       Justify : in Justify_Type := J_DEFAULT) is
   begin
      Driver.Stream.Start_Entity ("text");
      Driver.Stream.Write_Attribute ("style", "font-size:14px");
      Driver.Stream.Write_Attribute ("x", Driver.To_String (X));
      Driver.Stream.Write_Attribute ("y", Driver.To_String (Y + Driver.Line_Size));
      --  Driver.Stream.Start_Entity ("tspan");
      Driver.Stream.Write_Wide_String (Text);
      --  Driver.Stream.End_Entity ("tspan");
      Driver.Stream.End_Entity ("text");
   end Put_Wide;

   overriding
   procedure Put_Wide (Driver  : in out Driver_Type;
                       Box     : in Rect_Type;
                       Text    : in Wide_Wide_String;
                       Style   : in Style_Type;
                       Justify : in Justify_Type := J_DEFAULT) is
      Style_Info : constant Style_Info_Type := Driver.Styles (Style.Index);
      Format : constant Justify_Type :=
        (if Justify = J_DEFAULT then Style_Info.Justify else Justify);
   begin
      Clip (Driver, Box);
      if Format = J_LEFT or else Format = J_DEFAULT then
         Driver.Put_Wide (Box.X, Box.Y, Text, Style);
         return;
      end if;
      declare
         First : Natural := Text'First;
         Last  : Natural := Text'Last;
         Dim   : Dimension_Type;
      begin
         while First <= Last loop
            Dim := Driver.Get_Wide_Dimension (Text (First .. Last), Style);
            if Dim.W <= Box.W then
               if Format = J_RIGHT then
                  Driver.Put_Wide (Box.X + Box.W - Dim.W, Box.Y,
                                   Text (First .. Last), Style);
               else
                  Driver.Put_Wide (Box.X + (Box.W - Dim.W) / 2, Box.Y,
                                   Text (First .. Last), Style);
               end if;
               return;
            end if;

            if Format = J_RIGHT then
               First := First + 1;
            else
               First := First + 1;
               Last := Last - 1;
            end if;
         end loop;
      end;
   end Put_Wide;

   overriding
   procedure New_Line (Driver : in out Driver_Type) is
   begin
      Driver.Y_Offset := Driver.Y_Offset + Driver.Line_Size;
   end New_Line;

   overriding
   procedure New_Page (Driver : in out Driver_Type) is
   begin
      if Ada.Strings.Unbounded.Length (Driver.CSS) > 0 then
         Driver.Stream.Start_Entity ("style");
         Driver.Stream.Write_String (Ada.Strings.Unbounded.To_String (Driver.CSS));
         Driver.Stream.End_Entity ("style");
      end if;
      Driver.Stream.End_Entity ("svg");
   end New_Page;

   procedure Clear (Driver : in out Driver_Type) is
   begin
      Driver.Clip := Driver.Area;
      Driver.Y_Offset := 0;
   end Clear;

end PT.Drivers.SVG;
