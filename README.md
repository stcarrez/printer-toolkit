# Toolkit to write reports

[![Build Status](https://img.shields.io/endpoint?url=https://porion.vacs.fr/porion/api/v1/projects/printer-toolkit/badges/build.json)](https://porion.vacs.fr/porion/projects/view/printer-toolkit/summary)
[![Test Status](https://img.shields.io/endpoint?url=https://porion.vacs.fr/porion/api/v1/projects/printer-toolkit/badges/tests.json)](https://porion.vacs.fr/porion/projects/view/printer-toolkit/xunits)
[![Coverage](https://img.shields.io/endpoint?url=https://porion.vacs.fr/porion/api/v1/projects/printer-toolkit/badges/coverage.json)](https://porion.vacs.fr/porion/projects/view/printer-toolkit/summary)

Printer toolkit provides support to write reports on console or to produce SVG files with almost
the same Ada code.

![multi bars](examples/multi-bars.png)

![perf-plot](examples/perf-plot.png)

## Projects using printer_toolkit

* [Porion Build Manager](https://porion.vacs.fr/porion/index.html) for the command line console reports
* [spdx-tool](https://gitlab.com/stcarrez/spdx-tool) for the license and language reports

## Alire setup

The core library is provided by `printer_toolkit` and the SVG support implemented by the `printer_toolkit_svg`.
Run one of the following Alire commands or both:

```
alr with printer_toolkit
alr with printer_toolkit_svg
```

## Driver creation

In the printer toolkit library, the driver is responsible for building and creating
the report for a target support: the console, a PDF, an SVG file.  The console driver
is configured by getting the screen dimension, creating the driver instance and setting
the flush operation that writes the content on the console:

```
with PT.Drivers.Texts.GNAT_IO;

Screen     : constant PT.Dimension_Type := PT.Drivers.Texts.Screen_Dimension;
Driver     : PT.Drivers.Texts.Printer_Type := PT.Drivers.Texts.Create (Screen);
...
   Driver.Set_Flush (PT.Drivers.Texts.GNAT_IO.Flush'Access);
```

In this example, a `GNAT_IO` implementation is used so that it supports writing
UTF-8 character sequences correctly.

## Styles configuration

Before creating the report, we must define the style that indicates how the
output will look like.  The style includes the font color, the background,
how text fields a justified, the font family and so on.  The extract
below shows the creation of 3 styles each associated with different colors:

```
White      : constant PT.Style_Type := Driver.Create_Style (PT.Colors.White);
Green      : constant PT.Style_Type := Driver.Create_Style (PT.Colors.Green);
Red        : constant PT.Style_Type := Driver.Create_Style (PT.Colors.Red);
...
   Driver.Set_Fill (Red, PT.Drivers.Texts.F_FULL);
   Driver.Set_Fill (Green, PT.Drivers.Texts.F_FULL);
```

## Fields and layout

Very often, a report is composed of fields that are organised together in the page.
The library allows to represent each field and define some layout and style for each of
them.  Each field is described by a `Field_Type` instance which defines the style
and dimension expected for the field.  The dimension can be defined as an absolute
dimension or expressed as a percent of the page width.

```
Printer    : PT.Texts.Printer_Type := PT.Texts.Create (Driver);
Label      : PT.Texts.Field_Type;
Count1     : PT.Texts.Field_Type;
Count2     : PT.Texts.Field_Type;
Progress   : PT.Texts.Field_Type;
...
   Printer.Create_Field (Label, White, 20.0);
   Printer.Create_Field (Count1, Green, 10.0);
   Printer.Create_Field (Count2, Green, 10.0);
   Printer.Create_Field (Progress, Red, 60.0);
```

The above example created 4 fields with a percent width for each of them.
Within a field, it is possible to write text as well as progress bars (or other kind of items).
The field definition defines a clipping area based on the dimension allocated for the field.
The clipping area makes sure that what is printed in the field does not overlap with other
fields.

## Progress bars

Progress bars can be written in a field and the generic package `PT.Charts.Barchats` provides
a flexible support for that.  The package must be instantiated with a type that represent the
value to represent in the progress bar.  Because the value must be converted to a width
in the report, a function must be given in the instantiation to convert the value into such width.

To display progress bars that use a `Natural` as value, we could write:

```
function Percent is new PT.Charts.Width_Range (Natural);

package Draw_Natural_Bar is
   new PT.Charts.Barcharts (Natural, Percent => Percent);
```

## Writing the report

To write the report, each field is written by using one of the `Put`, `Put_Int`, `Put_Long`,
`Put_Wide` procedure and indicating the field being written.  The progress bar is
created by calling the `Draw_Progress` procedure of the instantiated generic package:

```
   Printer.Put (Label, Arg (Arg'First .. Sep1 - 1));
   Printer.Put_Int (Count1, 10);
   Printer.Put_Int (Count2, 15);
   Draw_Natural_Bar.Draw_Progress (Printer, Printer.Get_Box (Progress),
                                   10, 0, 10 + 15, Red, Green);
   Printer.New_Line;
```

## Example

To build the examples, use:

```
cd examples
alr build
```

- [colors.adb](https://gitlab.com/stcarrez/printer-toolkit/-/blob/main/examples/src/colors.adb?ref_type=heads)
- [simple_du.adb](https://gitlab.com/stcarrez/printer-toolkit/-/blob/main/examples/src/simple_du.adb?ref_type=heads)
- [simple_bar.adb](https://gitlab.com/stcarrez/printer-toolkit/-/blob/main/examples/src/simple_bar.adb?ref_type=heads)
- [multi.adb](https://gitlab.com/stcarrez/printer-toolkit/-/blob/main/examples/src/multi.adb?ref_type=heads)
- [multi_svg.adb](https://gitlab.com/stcarrez/printer-toolkit/-/blob/main/examples/src/multi_svg.adb?ref_type=heads)
- [multi_hbar.adb](https://gitlab.com/stcarrez/printer-toolkit/-/blob/main/examples/src/multi_hbar.adb?ref_type=heads)
- [plot_perf.adb](https://gitlab.com/stcarrez/printer-toolkit/-/blob/main/examples/src/plot_perf.adb?ref_type=heads)

## Misc notes

- The text report produces UTF-8 and ANSI Escape sequences.  On Windows, you must configure the terminal
  to use UTF-8 encoding (use `chcp 65001` or the`intl.cpl` tool).  The `mintty` supports ANSI escape sequence
  but Windows Powershell needs a manual setup.
