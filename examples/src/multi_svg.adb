with Util.Streams.Texts;
with Ada.Text_IO;
with PT.Drivers.SVG;
with PT.Texts;
with PT.Colors;
with PT.Charts.Barcharts;
procedure Multi_Svg is
   use type PT.W_Type;

   type Millisecond_Type is new Natural;

   function Percent is new PT.Charts.Width_Range (Millisecond_Type);

   package Millisecond_Barcharts is
      new PT.Charts.Barcharts (Millisecond_Type, Percent => Percent);

   Output    : aliased Util.Streams.Texts.Print_Stream;
   Driver    : PT.Drivers.SVG.Printer_Type;
begin
   Output.Initialize (Size => 32768);
   Driver.Create (Area => (0, 0, 2000, 200), Output => Output'Unchecked_Access);
   Driver.CSS_Style ("text { font-family: Arial }");
   declare
      Printer   : PT.Texts.Printer_Type := PT.Texts.Create (Driver);
      White     : constant PT.Style_Type := Driver.Create_Style (PT.Colors.White);
      Green     : constant PT.Style_Type := Driver.Create_Style (PT.Colors.Green);
      Red       : constant PT.Style_Type := Driver.Create_Style (PT.Colors.Red);
      Blue      : constant PT.Style_Type := Driver.Create_Style (PT.Colors.Blue);
      Title     : constant PT.Style_Type := Driver.Create_Style (PT.Colors.Yellow);
      Yellow    : constant PT.Style_Type := Driver.Create_Style (PT.Colors.Yellow);
      Label     : PT.Texts.Field_Type;
      Value     : PT.Texts.Field_Type;
      Progress1 : PT.Texts.Field_Type;
      Progress2 : PT.Texts.Field_Type;
      Progress3 : PT.Texts.Field_Type;
      Progress4 : PT.Texts.Field_Type;
      Progress5 : PT.Texts.Field_Type;
   begin
      Driver.Set_Justify (Title, PT.J_CENTER);
      Printer.Create_Field (Label, White, 20.0);
      Printer.Create_Field (Value, Green, 15.0);
      Printer.Create_Field (Progress1, Title, 10.0);
      Printer.Create_Field (Progress2, Title, 20.0);
      Printer.Create_Field (Progress3, Title, 15.0);
      Printer.Create_Field (Progress4, Title, 5.0);
      Printer.Create_Field (Progress5, Title, 5.0);

      Printer.Put (Label, "");
      Printer.Put (Value, "");
      Printer.Put (Progress1, "1");
      Printer.Put (Progress2, "2");
      Printer.Put (Progress3, "3");
      Printer.Put (Progress4, "4");
      Printer.Put (Progress5, "5");
      Printer.New_Line;

      Printer.Put (Label, "");
      Printer.Put (Value, "");
      declare
         Box : PT.Rect_Type := Printer.Get_Box (Progress1);
      begin
         Millisecond_Barcharts.Draw_Progress (Printer, Box,
                                              30000, 0, 30000, Green, Red);

         Box.X := Box.X + Box.W;
         Box.W := Printer.Get_Box (Progress2).W;
         Millisecond_Barcharts.Draw_Progress  (Printer, Box,
                                               30000, 0, 30000, Blue, Red);

         Box.X := Box.X + Box.W;
         Box.W := Printer.Get_Box (Progress3).W;
         Millisecond_Barcharts.Draw_Progress  (Printer, Box,
                                               30000, 0, 30000, Red, Red);

         Box.X := Box.X + Box.W;
         Box.W := Printer.Get_Box (Progress4).W;
         Millisecond_Barcharts.Draw_Progress  (Printer, Box,
                                               30000, 0, 30000, Yellow, Red);

         Box.X := Box.X + Box.W;
         Box.W := Printer.Get_Box (Progress5).W;
         Millisecond_Barcharts.Draw_Progress  (Printer, Box,
                                               30000, 0, 30000, Blue, Red);
      end;
      Printer.New_Line;

      Printer.Put (Label, "Build duration");
      Printer.Put (Value, "20.3");
      Printer.Put (Progress1, "3.4");
      Printer.Put (Progress2, "16.4");
      Printer.Put (Progress3, "8.2");
      Printer.Put (Progress4, "4.2");
      Printer.Put (Progress5, "3.2");
      Printer.New_Line;

      Printer.Create_Field (Progress2, Title, 65.0);
      Printer.Put (Label, "Time");
      Printer.Put (Value, "28.0 (user) 2.0 (sys)");
      declare
         Box : constant PT.Rect_Type := Printer.Get_Box (Progress2);
      begin
         Millisecond_Barcharts.Draw_Progress  (Printer, Box,
                                               2000, 0, 30000, Red, Green);
         Printer.New_Line;

         Printer.Put (Label, "Disk space:");
         Printer.Put (Value, "57K (+17%)");
         Millisecond_Barcharts.Draw_Progress  (Printer, Box,
                                               47, 0, 47 + 10, Green, Red);
         Printer.New_Line;

         Printer.Put (Label, "Files:");
         Printer.Put (Value, "43 (+23%)");
         Millisecond_Barcharts.Draw_Progress  (Printer, Box,
                                               33, 0, 33 + 10, Green, Red);
         Printer.New_Line;

         Printer.Put (Label, "Directories:");
         Printer.Put (Value, "51 (+41%)");
         Millisecond_Barcharts.Draw_Progress  (Printer, Box,
                                               30, 0, 30 + 21, Green, Red);
         Printer.New_Line;

         Printer.Put (Label, "Coverage lines:");
         Printer.Put (Value, " (81.6%)");
         Millisecond_Barcharts.Draw_Progress  (Printer, Box,
                                               7535, 0, 7535 + 1703, Green, Red);
         Printer.New_Line;

         Printer.Put (Label, "Coverage functions:");
         Printer.Put (Value, " (71.0%)");
         Millisecond_Barcharts.Draw_Progress  (Printer, Box,
                                               988, 0, 988 + 404, Green, Red);
         Printer.New_Line;

         Printer.New_Page;
      end;
   end;
   Ada.Text_IO.Put_Line (Util.Streams.Texts.To_String (Output));
end Multi_Svg;
