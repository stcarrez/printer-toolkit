with Ada.Command_Line;
with Ada.Strings.Fixed;
with Ada.Text_IO;
with PT.Drivers.Texts;
with PT.Drivers.Texts.GNAT_IO;
with PT.Texts;
with PT.Colors;
with PT.Charts.Barcharts;
procedure Progress is

   function Percent is new PT.Charts.Width_Range (Natural);

   package Draw_Natural_Bar is
      new PT.Charts.Barcharts (Natural, Percent => Percent);

   Screen     : constant PT.Dimension_Type := PT.Drivers.Texts.Screen_Dimension;
   Driver     : PT.Drivers.Texts.Printer_Type := PT.Drivers.Texts.Create (Screen);
   Printer    : PT.Texts.Printer_Type := PT.Texts.Create (Driver);
   White      : constant PT.Style_Type := Driver.Create_Style (PT.Colors.White);
   Green      : constant PT.Style_Type := Driver.Create_Style (PT.Colors.Green);
   Red        : constant PT.Style_Type := Driver.Create_Style (PT.Colors.Red);
   Label      : PT.Texts.Field_Type;
   Count1     : PT.Texts.Field_Type;
   Count2     : PT.Texts.Field_Type;
   Progress   : PT.Texts.Field_Type;
   Count      : constant Natural := Ada.Command_Line.Argument_Count;
begin
   if Count = 0 then
      Ada.Text_IO.Put_Line ("Usage: progress {label:value:value} ...");
      return;
   end if;
   Driver.Set_Flush (PT.Drivers.Texts.GNAT_IO.Flush'Access);
   Driver.Set_Fill (Red, PT.Drivers.Texts.F_FULL);
   Driver.Set_Fill (Green, PT.Drivers.Texts.F_FULL);
   Printer.Create_Field (Label, White, 20.0);
   Printer.Create_Field (Count1, Green, 10.0);
   Printer.Create_Field (Count2, Green, 10.0);
   Printer.Create_Field (Progress, Red, 60.0);

   for I in 1 .. Count loop
      declare
         Arg : constant String := Ada.Command_Line.Argument (I);
         Sep1, Sep2 : Natural;
         Val1, Val2 : Natural;
      begin
         Sep1 := Ada.Strings.Fixed.Index (Arg, ":");
         Sep2 := Ada.Strings.Fixed.Index (Arg, ":", Sep1 + 1);
         Val1 := Natural'Value (Arg (Sep1 + 1 .. Sep2 - 1));
         Val2 := Natural'Value (Arg (Sep2 + 1 .. Arg'Last));
         Printer.Put (Label, Arg (Arg'First .. Sep1 - 1));
         Printer.Put_Int (Count1, Val1);
         Printer.Put_Int (Count2, Val2);
         Draw_Natural_Bar.Draw_Progress (Printer, Printer.Get_Box (Progress),
                                         Val1, 0, Val1 + Val2, Red, Green);
         Printer.New_Line;

      exception
         when others =>
            null;
      end;
   end loop;
end Progress;
