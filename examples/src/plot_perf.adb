with Ada.Command_Line;
with Ada.Containers.Vectors;
with PT.Drivers.Texts;
with PT.Drivers.Texts.GNAT_IO;
with PT.Texts;
with PT.Colors;
with PT.Charts.Barcharts;
with Util.Strings;
with Util.Beans;
with Util.Beans.Objects;
with Util.Serialize.Mappers.Record_Mapper;
with Util.Serialize.IO.XML;
procedure Plot_Perf is

   package UBO renames Util.Beans.Objects;
   use type UBO.Object;

   type Measure_Type is record
      Title  : UBO.Object;
      Time   : Duration;
      Count  : Natural;
      Errors : Natural;
   end record;

   function Format (D : in Duration) return String;
   function To_Duration (Value : in UBO.Object) return Duration;

   function "<" (Left, Right : Measure_Type)
                 return Boolean is (Left.Count < Right.Count
                                    or else (Left.Count = Right.Count
                                      and then Left.Title < Right.Title));

   package Measure_Vectors is
      new Ada.Containers.Vectors (Element_Type => Measure_Type, Index_Type => Positive);

   package Sort is
      new Measure_Vectors.Generic_Sorting ("<");

   type Measure_Field is (FIELD_COUNT, FIELD_TIME, FIELD_TITLE,
                          FIELD_ERRORS, FIELD_FAILURES, FIELD_PERF);

   type Measures_Type is record
      Measures : Measure_Vectors.Vector;
      Count    : UBO.Object;
      Title    : UBO.Object;
      Time     : UBO.Object;
      Errors   : UBO.Object;
      Failures : UBO.Object;
      Tot_Time : Duration := 0.0;
      Max_Time : Duration := 0.0;
      Max_Length : Natural := 0;
      Max_Count  : Natural := 0;
   end record;
   type Measures_Access is access all Measures_Type;

   procedure Set_Member (P     : in out Measures_Type;
                         Field : in Measure_Field;
                         Value : in UBO.Object);

   function To_Duration (Value : in UBO.Object) return Duration is
      S   : constant String := UBO.To_String (Value);
      Sep : constant Natural := Util.Strings.Index (S, ' ');
      Result : Duration;
   begin
      if Sep = 0 then
         return Duration'Value (S);
      end if;
      Result := Duration'Value (S (S'First .. Sep - 1));
      if S (Sep .. S'Last) = " ns" then
         Result := Result / 1_000_000_000;
      elsif S (Sep .. S'Last) = " us" then
         Result := Result / 1_000_000;
      elsif S (Sep .. S'Last) = " ms" then
         Result := Result / 1_000;
      end if;
      return Result;

   exception
      when others =>
         return 0.0;
   end To_Duration;

   function Format (D : in Duration) return String is
   begin
      if D < 0.000_001 then
         return Duration'Image (D * 1_000_000_000) (1 .. 6) & " ns";
      elsif D < 0.001 then
         return Duration'Image (D * 1_000_000) (1 .. 6) & " us";
      elsif D < 1.0 then
         return Duration'Image (D * 1_000) (1 .. 6) & " ms";
      else
         return Duration'Image (D) (1 .. 6) & " s";
      end if;
   end Format;

   procedure Set_Member (P     : in out Measures_Type;
                         Field : in Measure_Field;
                         Value : in UBO.Object) is
   begin
      case Field is
         when FIELD_COUNT =>
            P.Count := Value;

         when FIELD_TIME =>
            P.Time := Value;

         when FIELD_TITLE =>
            P.Title := Value;

         when FIELD_ERRORS =>
            P.Errors := Value;

         when FIELD_FAILURES =>
            P.Failures := Value;

         when FIELD_PERF =>
            if not UBO.Is_Null (P.Title) and then not UBO.Is_Null (P.Time) then
               declare
                  Title  : constant String := UBO.To_String (P.Title);
                  Time   : constant Duration := To_Duration (P.Time);
                  Count  : constant Integer := UBO.To_Integer (P.Count);
                  Errors : constant Integer := UBO.To_Integer (P.Errors);
                  Fail   : constant Integer := UBO.To_Integer (P.Failures);
               begin
                  P.Measures.Append ((Title => P.Title, Time => Time,
                                      Count => Count, Errors => Errors + Fail));
                  if P.Max_Length < Title'Length then
                     P.Max_Length := Title'Length;
                  end if;
                  P.Tot_Time := P.Tot_Time + Time;
                  if P.Max_Time < Time then
                     P.Max_Time := Time;
                  end if;
                  if P.Max_Count < Count then
                     P.Max_Count := Count;
                  end if;
               end;
            end if;
            P.Count := UBO.Null_Object;
            P.Time := UBO.Null_Object;
            P.Title := UBO.Null_Object;
            P.Errors := UBO.Null_Object;
            P.Failures := UBO.Null_Object;

      end case;
   end Set_Member;

   package Measure_Mapper is
     new Util.Serialize.Mappers.Record_Mapper (Element_Type        => Measures_Type,
                                               Element_Type_Access => Measures_Access,
                                               Fields              => Measure_Field,
                                               Set_Member          => Set_Member);

   function Percent is new PT.Charts.Width_Range (Natural);

   package Draw_Count_Bar is
      new PT.Charts.Barcharts (Natural, Percent => Percent);

   function Percent is new PT.Charts.Width_Fixed (Duration);

   package Draw_Duration_Bar is
      new PT.Charts.Barcharts (Duration, Percent => Percent);

   Screen     : constant PT.Dimension_Type := PT.Drivers.Texts.Screen_Dimension;
   Driver     : PT.Drivers.Texts.Printer_Type := PT.Drivers.Texts.Create (Screen);
   Printer    : PT.Texts.Printer_Type := PT.Texts.Create (Driver);
   White      : constant PT.Style_Type := Driver.Create_Style (PT.Colors.White);
   Green      : constant PT.Style_Type := Driver.Create_Style (PT.Colors.Green);
   Red        : constant PT.Style_Type := Driver.Create_Style (PT.Colors.Red);
   Title      : constant PT.Style_Type := Driver.Create_Style (PT.Colors.White);
   Fields     : PT.Texts.Field_Array (1 .. 6);
   Reader           : Util.Serialize.IO.XML.Parser;
   Measure_Mapping  : aliased Measure_Mapper.Mapper;
   Mapper           : Util.Serialize.Mappers.Processing;
   Count  : constant Natural := Ada.Command_Line.Argument_Count;
   Data : aliased Measures_Type;
begin
   Driver.Set_Flush (PT.Drivers.Texts.GNAT_IO.Flush'Access);
   Driver.Set_Fill (Red, PT.Drivers.Texts.F_FULL);
   Driver.Set_Fill (Green, PT.Drivers.Texts.F_FULL);
   if Count = 0 then
      Printer.Put ("Usage: plot_perf file.xml...");
      Printer.New_Line;
      Printer.Put ("Print summary of execution of unit tests "
                   & "implemented with Ada Utility Library");
      Printer.New_Line;
      return;
   end if;
   Measure_Mapping.Add_Mapping ("time/@count", FIELD_COUNT);
   Measure_Mapping.Add_Mapping ("time/@total", FIELD_TIME);
   Measure_Mapping.Add_Mapping ("time/@title", FIELD_TITLE);
   Measure_Mapping.Add_Mapping ("time", FIELD_PERF);
   Measure_Mapping.Add_Mapping ("testcase/@tests", FIELD_COUNT);
   Measure_Mapping.Add_Mapping ("testcase/@time", FIELD_TIME);
   Measure_Mapping.Add_Mapping ("testcase/@name", FIELD_TITLE);
   Measure_Mapping.Add_Mapping ("testcase/@errors", FIELD_ERRORS);
   Measure_Mapping.Add_Mapping ("testcase/@failures", FIELD_FAILURES);
   Measure_Mapping.Add_Mapping ("testcase", FIELD_PERF);
   Mapper.Add_Mapping ("measures", Measure_Mapping'Unchecked_Access);
   Mapper.Add_Mapping ("testsuite", Measure_Mapping'Unchecked_Access);

   for I in 1 .. Count loop
      declare
         S    : constant String := Ada.Command_Line.Argument (I);
      begin
         Measure_Mapper.Set_Context (Mapper, Data'Unchecked_Access);
         Reader.Parse (S, Mapper);
      end;
   end loop;

   Sort.Sort (Data.Measures);
   if Data.Max_Length = 0 then
      Data.Max_Length := 50;
   end if;
   Driver.Set_Font (Title, PT.F_BOLD);
   Printer.Create_Field (Fields (1), Title, 0.0);
   Printer.Set_Max_Dimension (Fields (1), (W => PT.X_Type (Data.Max_Length),
                                           H => 1));
   Printer.Create_Field (Fields (2), Title, (W => 10, H => 1));
   Printer.Create_Field (Fields (3), Title, 10.0);
   Printer.Create_Field (Fields (4), Title, 10.0);
   Printer.Create_Field (Fields (5), Title, 20.0);
   Printer.Set_Top_Left_Padding (Fields (1), (W => 1, H => 0));
   Printer.Set_Top_Left_Padding (Fields (2), (W => 1, H => 0));
   Printer.Set_Top_Left_Padding (Fields (3), (W => 1, H => 0));
   Printer.Set_Top_Left_Padding (Fields (4), (W => 1, H => 0));
   Printer.Layout_Fields (Fields);
   declare
      W : PT.W_Type;
   begin
      Printer.Put (Fields (1), "Test name");
      Printer.Put (Fields (2), "Duration");
      Printer.Put (Fields (3), "");
      Printer.Put (Fields (4), "Tests");
      Printer.New_Line;
      Printer.Set_Style (Fields (1), White);
      Printer.Set_Style (Fields (2), White);
      Printer.Set_Style (Fields (3), White);
      Printer.Set_Style (Fields (4), White);
      for M of Data.Measures loop
         Printer.Put (Fields (1), UBO.To_String (M.Title));
         Printer.Put (Fields (2), Format (M.Time));
         Draw_Duration_Bar.Draw_Bar (Printer, Printer.Get_Box (Fields (3)),
                                     M.Time, 0.0, Data.Max_Time,
                                     Green, W);
         Printer.Set_Position (Fields (4));
         declare
            use type PT.X_Type;
            Box : PT.Rect_Type := Printer.Get_Box (Fields (4));
         begin
            if M.Errors > 0 then
               Draw_Count_Bar.Draw_Bar (Printer, Box,
                                        M.Errors, 0, Data.Max_Count,
                                        Red, W);
               Box.X := Box.X + W;
            end if;
            Draw_Count_Bar.Draw_Bar (Printer, Box,
                                     M.Count - M.Errors, 0, Data.Max_Count,
                                     Green, W);
         end;
         Printer.Put (Fields (5), M.Count'Image);
         Printer.New_Line;
      end loop;
   end;
end Plot_Perf;
