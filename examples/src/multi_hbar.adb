with PT.Drivers.Texts;
with PT.Drivers.Texts.GNAT_IO;
with PT.Texts;
with PT.Colors;
with PT.Charts.Barcharts;
procedure Multi_HBar is
   use type PT.W_Type;

   type Millisecond_Type is new Natural;
   function Format (Value : in Millisecond_Type) return String;

   function Percent is new PT.Charts.Width_Range (Millisecond_Type);

   function Format (Value : in Millisecond_Type) return String is
   begin
      return Millisecond_Type'Image (Value);
   end Format;

   package MS_Barcharts is
      new PT.Charts.Barcharts (Value_Type => Millisecond_Type);

   Screen    : constant PT.Dimension_Type := PT.Drivers.Texts.Screen_Dimension;
   Driver    : PT.Drivers.Texts.Printer_Type := PT.Drivers.Texts.Create (Screen);
   Printer   : PT.Texts.Printer_Type := PT.Texts.Create (Driver);
   Chart     : PT.Charts.Printer_Type := PT.Charts.Create (Driver, 0);
   Values    : MS_Barcharts.Label_Value_Array (1 .. 5);
begin
   Driver.Set_Flush (PT.Drivers.Texts.GNAT_IO.Flush'Access);
   Values (1).Label := PT.To_UString ("1");
   Values (1).Value := 132;
   Values (1).Style := Chart.Create_Style (PT.Colors.White);

   Values (2).Label := PT.To_UString ("2");
   Values (2).Value := 1320;
   Values (2).Style := Chart.Create_Style (PT.Colors.Red);

   Values (3).Label := PT.To_UString ("3");
   Values (3).Value := 13020;
   Values (3).Style := Chart.Create_Style (PT.Colors.Green);

   Values (4).Label := PT.To_UString ("4");
   Values (4).Value := 89320;
   Values (4).Style := Chart.Create_Style (PT.Colors.Blue);

   Values (5).Label := PT.To_UString ("5");
   Values (5).Value := 8930;
   Values (5).Style := Chart.Create_Style (PT.Colors.Yellow);

   MS_Barcharts.Draw_Horizontal_Stacked (Chart, (20, 1, 80, 1), Values, 0, Format'Access);
   Printer.Put ((1, 1, 20, 1), Values (1).Style, "Execution time");
   Printer.New_Line;

end Multi_HBar;
