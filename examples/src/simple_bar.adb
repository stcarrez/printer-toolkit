with PT.Drivers.Texts;
with PT.Drivers.Texts.GNAT_IO;
with PT.Texts;
with PT.Colors;
with PT.Charts.Barcharts;
procedure Simple_Bar is
   type Millisecond_Type is new Natural;

   function Percent is new PT.Charts.Width_Range (Millisecond_Type);

   package Millisecond_Barcharts is
      new PT.Charts.Barcharts (Millisecond_Type, Percent => Percent);

   Screen    : constant PT.Dimension_Type := PT.Drivers.Texts.Screen_Dimension;
   Driver    : PT.Drivers.Texts.Printer_Type := PT.Drivers.Texts.Create (Screen);
   Printer   : PT.Texts.Printer_Type := PT.Texts.Create (Driver);
   White     : constant PT.Style_Type := Driver.Create_Style (PT.Colors.White);
   Black     : constant PT.Style_Type := Driver.Create_Style (PT.Colors.Black);
   Green     : constant PT.Style_Type := Driver.Create_Style (PT.Colors.Green);
   Red       : constant PT.Style_Type := Driver.Create_Style (PT.Colors.Red);
   Label     : PT.Texts.Field_Type;
   Value     : PT.Texts.Field_Type;
   Progress  : PT.Texts.Field_Type;
begin
   Driver.Set_Flush (PT.Drivers.Texts.GNAT_IO.Flush'Access);
   Driver.Set_Fill (Red, PT.Drivers.Texts.F_MED1);
   Driver.Set_Fill (Green, PT.Drivers.Texts.F_MED1);
   Printer.Create_Field (Label, White, 20.0);
   Printer.Create_Field (Value, Green, 30.0);
   Printer.Create_Field (Progress, Red, 60.0);

   Printer.Put (Label, "Duration");
   Printer.Put (Value, "20.3");
   Millisecond_Barcharts.Draw_Progress (Printer, Printer.Get_Box (Progress),
                                        20300, 0, 30000, Green, Black);
   Printer.New_Line;

   Printer.Put (Label, "Time");
   Printer.Put (Value, "28.0 (user) 2.0 (sys)");
   Millisecond_Barcharts.Draw_Progress (Printer, Printer.Get_Box (Progress),
                                        2000, 0, 30000, Red, Green);
   Printer.New_Line;
   Printer.New_Line;

   Printer.Put_Wide (Green, PT.Drivers.Texts.F_UPPER
                     & PT.Drivers.Texts.F_THICK_UPPER
                     & PT.Drivers.Texts.F_LOWER
                     & PT.Drivers.Texts.F_LOW1
                     & PT.Drivers.Texts.F_LOW2
                     & PT.Drivers.Texts.F_LOW3
                     & PT.Drivers.Texts.F_MED1
                     & PT.Drivers.Texts.F_MED2
                     & PT.Drivers.Texts.F_MED3
                     & PT.Drivers.Texts.F_FULL);
   Printer.New_Line;

   Driver.Set_Fill (Red, PT.Drivers.Texts.F_UPPER);
   Driver.Set_Fill (Green, PT.Drivers.Texts.F_UPPER);
   Printer.Put (Label, "F_UPPER");
   Printer.Put (Value, "28.0 (user) 2.0 (sys)");
   Millisecond_Barcharts.Draw_Progress (Printer, Printer.Get_Box (Progress),
                                        2000, 0, 30000, Red, Green);
   Printer.New_Line;

   Driver.Set_Fill (Red, PT.Drivers.Texts.F_THICK_UPPER);
   Driver.Set_Fill (Green, PT.Drivers.Texts.F_THICK_UPPER);
   Printer.Put (Label, "F_THICK_UPPER");
   Printer.Put (Value, "28.0 (user) 2.0 (sys)");
   Millisecond_Barcharts.Draw_Progress (Printer, Printer.Get_Box (Progress),
                                        2000, 0, 30000, Red, Green);
   Printer.New_Line;

   Driver.Set_Fill (Red, PT.Drivers.Texts.F_LOWER);
   Driver.Set_Fill (Green, PT.Drivers.Texts.F_LOWER);
   Printer.Put (Label, "F_LOWER");
   Printer.Put (Value, "28.0 (user) 2.0 (sys)");
   Millisecond_Barcharts.Draw_Progress (Printer, Printer.Get_Box (Progress),
                                        2000, 0, 30000, Red, Green);
   Printer.New_Line;

   Driver.Set_Fill (Red, PT.Drivers.Texts.F_LOW1);
   Driver.Set_Fill (Green, PT.Drivers.Texts.F_LOW1);
   Printer.Put (Label, "F_LOW1");
   Printer.Put (Value, "28.0 (user) 2.0 (sys)");
   Millisecond_Barcharts.Draw_Progress (Printer, Printer.Get_Box (Progress),
                                        2000, 0, 30000, Red, Green);
   Printer.New_Line;

   Driver.Set_Fill (Red, PT.Drivers.Texts.F_LOW2);
   Driver.Set_Fill (Green, PT.Drivers.Texts.F_LOW2);
   Printer.Put (Label, "F_LOW2");
   Printer.Put (Value, "28.0 (user) 2.0 (sys)");
   Millisecond_Barcharts.Draw_Progress (Printer, Printer.Get_Box (Progress),
                                        2000, 0, 30000, Red, Green);
   Printer.New_Line;

   Driver.Set_Fill (Red, PT.Drivers.Texts.F_LOW3);
   Driver.Set_Fill (Green, PT.Drivers.Texts.F_LOW3);
   Printer.Put (Label, "F_LOW3");
   Printer.Put (Value, "28.0 (user) 2.0 (sys)");
   Millisecond_Barcharts.Draw_Progress (Printer, Printer.Get_Box (Progress),
                                        2000, 0, 30000, Red, Green);
   Printer.New_Line;

   Driver.Set_Fill (Red, PT.Drivers.Texts.F_MED1);
   Driver.Set_Fill (Green, PT.Drivers.Texts.F_MED1);
   Printer.Put (Label, "F_MED1");
   Printer.Put (Value, "28.0 (user) 2.0 (sys)");
   Millisecond_Barcharts.Draw_Progress (Printer, Printer.Get_Box (Progress),
                                        2000, 0, 30000, Red, Green);
   Printer.New_Line;

   Driver.Set_Fill (Red, PT.Drivers.Texts.F_MED2);
   Driver.Set_Fill (Green, PT.Drivers.Texts.F_MED2);
   Printer.Put (Label, "F_MED2");
   Printer.Put (Value, "28.0 (user) 2.0 (sys)");
   Millisecond_Barcharts.Draw_Progress (Printer, Printer.Get_Box (Progress),
                                        2000, 0, 30000, Red, Green);
   Printer.New_Line;

   Driver.Set_Fill (Red, PT.Drivers.Texts.F_MED3);
   Driver.Set_Fill (Green, PT.Drivers.Texts.F_MED3);
   Printer.Put (Label, "F_MED3");
   Printer.Put (Value, "28.0 (user) 2.0 (sys)");
   Millisecond_Barcharts.Draw_Progress (Printer, Printer.Get_Box (Progress),
                                        2000, 0, 30000, Red, Green);
   Printer.New_Line;

   Driver.Set_Fill (Red, PT.Drivers.Texts.F_FULL);
   Driver.Set_Fill (Green, PT.Drivers.Texts.F_FULL);
   Printer.Put (Label, "F_FULL");
   Printer.Put (Value, "28.0 (user) 2.0 (sys)");
   Millisecond_Barcharts.Draw_Progress (Printer, Printer.Get_Box (Progress),
                                        2000, 0, 30000, Red, Green);
   Printer.New_Line;

   Driver.Set_Fill (Red, PT.Drivers.Texts.F_HLINE1);
   Driver.Set_Fill (Green, PT.Drivers.Texts.F_HLINE1);
   Printer.Put (Label, "F_HLINE1");
   Printer.Put (Value, "28.0 (user) 2.0 (sys)");
   Millisecond_Barcharts.Draw_Progress (Printer, Printer.Get_Box (Progress),
                                        2000, 0, 30000, Red, Green);
   Printer.New_Line;

   Driver.Set_Fill (Red, PT.Drivers.Texts.F_HLINE2);
   Driver.Set_Fill (Green, PT.Drivers.Texts.F_HLINE2);
   Printer.Put (Label, "F_HLINE2");
   Printer.Put (Value, "28.0 (user) 2.0 (sys)");
   Millisecond_Barcharts.Draw_Progress (Printer, Printer.Get_Box (Progress),
                                        2000, 0, 30000, Red, Green);
   Printer.New_Line;

end Simple_Bar;
