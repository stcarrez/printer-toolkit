with PT.Drivers.Texts;
with PT.Drivers.Texts.GNAT_IO;
with PT.Texts;
with PT.Colors;
with AnsiAda;
with Ada.Text_IO;
procedure Colors is
   use PT;

   package ANSI renames AnsiAda;

   Driver    : PT.Drivers.Texts.Printer_Type := PT.Drivers.Texts.Create (Width => 80);
   Printer   : PT.Texts.Printer_Type := PT.Texts.Create (Driver);

   C : constant String := PT.Drivers.Texts.To_String (PT.Drivers.Texts.F_STRONG & "");
begin
   Driver.Set_Flush (PT.Drivers.Texts.GNAT_IO.Flush'Access);
   declare
      use PT.Colors;
      H : PT.Colors.Hue := 0.0;
      C : PT.Color_Type;
      N : Natural;
      S : PT.Style_Array (1 .. 60);
   begin
      for I in S'Range loop
         S (I) := Driver.Create_Style (PT.Colors.Black);
         Driver.Set_Fill (S (I), PT.Drivers.Texts.F_STRONG);
      end loop;
      while H <= 360.0 - 1.0 loop
         H := H + 1.0;
         N := (Natural (H) mod 60) + 1;
         C := PT.Colors.To_Color (H, 1.0, 1.0);
         Driver.Set_Color (S (N), C);
         Printer.Set_Style (S (N));
         Printer.Put_Wide (PT.Drivers.Texts.F_STRONG & "");
         if N = 60 then
            Printer.New_Line;
         end if;
      end loop;
   end;

   for B in 0 .. 31 loop
      Ada.Text_IO.Put (ANSI.Color_Wrap (C,
                       ANSI.Foreground (B * 8, B * 4, B * 4)));
   end loop;
   --  Grey colors
   for B in 0 .. 31 loop
      Ada.Text_IO.Put (ANSI.Color_Wrap (C,
                       ANSI.Foreground (B * 8, B * 8, B * 8)));
   end loop;
end Colors;
