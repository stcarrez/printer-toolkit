with Ada.Directories;
with PT.Drivers.Texts;
with PT.Drivers.Texts.GNAT_IO;
with PT.Texts;
with PT.Colors;
with PT.Charts.Barcharts;
procedure Simple_Du is

   subtype File_Size is Ada.Directories.File_Size;

   type Tree_Stat_Type is record
      File_Count : Natural := 0;
      Dir_Count  : Natural := 0;
      Total_Size : File_Size := 0;
   end record;

   procedure Find_Stats (Path  : in String;
                         Stats : in out Tree_Stat_Type);

   procedure Find_Stats (Path  : in String;
                         Stats : in out Tree_Stat_Type) is
      use Ada.Directories;

      Filter  : constant Filter_Type := (Ordinary_File => True,
                                         Directory     => True,
                                         others        => False);
      Search  : Search_Type;
      Ent     : Ada.Directories.Directory_Entry_Type;
   begin
      Start_Search (Search, Directory => Path,
                    Pattern => "*", Filter => Filter);
      while More_Entries (Search) loop
         Get_Next_Entry (Search, Ent);
         declare
            Name      : constant String := Simple_Name (Ent);
            Full_Path : constant String := Ada.Directories.Full_Name (Ent);
            Kind      : constant File_Kind := Ada.Directories.Kind (Ent);
         begin
            if Name /= "." and Name /= ".." then
               if Kind = Ada.Directories.Directory then
                  Stats.Dir_Count := Stats.Dir_Count + 1;
                  Find_Stats (Full_Path, Stats);
               elsif Kind = Ada.Directories.Ordinary_File then
                  Stats.File_Count := Stats.File_Count + 1;
                  Stats.Total_Size := Stats.Total_Size + Ada.Directories.Size (Ent);
               else
                  Stats.File_Count := Stats.File_Count + 1;
               end if;
            end if;
         end;
      end loop;
   end Find_Stats;

   function Percent is new PT.Charts.Width_Range (Natural);

   package Draw_Natural_Bar is
      new PT.Charts.Barcharts (Natural, Percent => Percent);

   Screen     : constant PT.Dimension_Type := PT.Drivers.Texts.Screen_Dimension;
   Driver     : PT.Drivers.Texts.Printer_Type := PT.Drivers.Texts.Create (Screen);
   Printer    : PT.Texts.Printer_Type := PT.Texts.Create (Driver);
   White      : constant PT.Style_Type := Driver.Create_Style (PT.Colors.White);
   Green      : constant PT.Style_Type := Driver.Create_Style (PT.Colors.Green);
   Red        : constant PT.Style_Type := Driver.Create_Style (PT.Colors.Red);
   Label      : PT.Texts.Field_Type;
   Dir_Count  : PT.Texts.Field_Type;
   File_Count : PT.Texts.Field_Type;
   Progress   : PT.Texts.Field_Type;
   Stats      : Tree_Stat_Type;
begin
   Driver.Set_Flush (PT.Drivers.Texts.GNAT_IO.Flush'Access);
   Driver.Set_Fill (Red, PT.Drivers.Texts.F_FULL);
   Driver.Set_Fill (Green, PT.Drivers.Texts.F_FULL);
   Printer.Create_Field (Label, White, 20.0);
   Printer.Create_Field (Dir_Count, Green, 10.0);
   Printer.Create_Field (File_Count, Green, 10.0);
   Printer.Create_Field (Progress, Red, 60.0);
   Find_Stats (".", Stats);

   Printer.Put (Label, "");
   Printer.Put (Dir_Count, "Dir");
   Printer.Put (File_Count, "Files");
   Printer.New_Line;

   Printer.Put (Label, "Disk usage");
   Printer.Put (Dir_Count, Stats.Dir_Count'Image);
   Printer.Put (File_Count, Stats.File_Count'Image);
   Draw_Natural_Bar.Draw_Progress (Printer, Printer.Get_Box (Progress),
                                   Stats.Dir_Count, 0, Stats.File_Count + Stats.Dir_Count,
                                   Red, Green);
   Printer.New_Line;
end Simple_Du;
