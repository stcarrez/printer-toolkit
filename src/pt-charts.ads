-----------------------------------------------------------------------
--  pt-charts -- Printing charts
--  Copyright (C) 2021, 2023 Stephane Carrez
--  Written by Stephane Carrez (Stephane.Carrez@gmail.com)
--  SPDX-License-Identifier: Apache-2.0
-----------------------------------------------------------------------

with Ada.Containers.Indefinite_Ordered_Maps;
package PT.Charts is

   type Sample_Value is new Integer;

   type Sample_Array is array (Positive range <>) of Sample_Value;

   type Sample_Data (Len : Positive) is record
      Values : Sample_Array (1 .. Len);
      Styles : Style_Array (1 .. Len);
   end record;

   package Sample_Maps is
     new Ada.Containers.Indefinite_Ordered_Maps (Key_Type => String,
                                                 Element_Type => Sample_Data);

   subtype Sample_Map is Sample_Maps.Map;
   subtype Sample_Cursor is Sample_Maps.Cursor;

   type Printer_Type is limited new PT.Printer_Type with private;

   --  Create the chart printer and configure it with the given width.
   function Create (From  : in PT.Printer_Type'Class;
                    Width : in W_Type := 0) return Printer_Type;

   procedure Draw_Barchart (Printer     : in out Printer_Type;
                            Label_Style : in Style_Type;
                            Samples     : in Sample_Map);

   --  Fill the rectangle [Rect.X, Rect.Y, Width, Rect.W] with the Style1
   --  color and the rest of the area with the Style2.
   procedure Fill (Printer : in out PT.Printer_Type'Class;
                   Rect    : in Rect_Type;
                   Width   : in W_Type;
                   Style1  : in Style_Type;
                   Style2  : in Style_Type) with
     Pre => Printer.Is_Open and Width <= Rect.W;

   --  Draw a progress bar in the given rectangle to show the position
   --  of the value within the min/max value range.
   generic
      type Value_Type is (<>);
      with function "-" (Left, Right : in Value_Type) return W_Type is <>;
   procedure Draw_Bar (Printer : in out Printer_Type;
                       Rect    : in Rect_Type;
                       Value   : in Value_Type;
                       Min     : in Value_Type := Value_Type'First;
                       Max     : in Value_Type := Value_Type'Last;
                       Style1  : in Style_Type;
                       Style2  : in Style_Type) with
     Pre => Printer.Is_Open and Min <= Max and Value >= Min and Value <= Max;

   generic
      type Value_Type is delta <>;
   function Width_Fixed (Width : in W_Type;
                         Value, Min, Max : in Value_Type) return W_Type;
   generic
      type Value_Type is range <>;
   function Width_Range (Width : in W_Type;
                         Value, Min, Max : in Value_Type) return W_Type;

private

   type Printer_Type is limited new PT.Printer_Type with record
      Column_Count : Natural := 0;
   end record;

end PT.Charts;
