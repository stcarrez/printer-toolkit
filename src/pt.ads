-----------------------------------------------------------------------
--  pt -- Printer Toolkit
--  Copyright (C) 2021, 2023, 2024 Stephane Carrez
--  Written by Stephane Carrez (Stephane.Carrez@gmail.com)
--  SPDX-License-Identifier: Apache-2.0
-----------------------------------------------------------------------
with Ada.Characters.Conversions;
with Ada.Strings.UTF_Encoding.Wide_Wide_Strings;
with Ada.Strings.Unbounded;
with Ada.Finalization;
package PT with Preelaborate is

   subtype UString is Ada.Strings.Unbounded.Unbounded_String;

   subtype WChar is Wide_Wide_Character;
   subtype WString is Wide_Wide_String;

   function To_WChar (C : in Character) return WChar
                      renames Ada.Characters.Conversions.To_Wide_Wide_Character;
   function To_WString (S : in String) return WString
                        renames Ada.Strings.UTF_Encoding.Wide_Wide_Strings.Decode;

   --  Coordinate system normalized to integers, the real unit depends on the driver
   --  (pixel, point, ...).
   type X_Type is new Natural;
   type Y_Type is new Natural;
   subtype W_Type is X_Type;
   subtype H_Type is Y_Type;

   function Max (Left, Right : X_Type) return X_Type is
     (if Left < Right then Right else Left);

   function Min (Left, Right : X_Type) return X_Type is
     (if Left < Right then Left else Right);

   function Max (Left, Right : Y_Type) return Y_Type is
     (if Left < Right then Right else Left);

   function Min (Left, Right : Y_Type) return Y_Type is
     (if Left < Right then Left else Right);

   type Rect_Type is record
      X : X_Type := 0;
      Y : Y_Type := 0;
      W : W_Type := 0;
      H : H_Type := 0;
   end record;
   function "&" (Left, Right : Rect_Type) return Rect_Type;

   type Position_Type is record
      X : X_Type := 0;
      Y : Y_Type := 0;
   end record;

   type Dimension_Type is record
      W : W_Type := 0;
      H : H_Type := 0;
   end record;

   function "+" (Left, Right : in Dimension_Type) return Dimension_Type is
      ((Left.W + Right.W, Left.H + Right.H));

   type Percent_Type is delta 0.01 range 0.0 .. 100.0;

   type Justify_Type is (J_DEFAULT, J_LEFT, J_RIGHT, J_CENTER);

   type Font_Type is (F_NORMAL, F_BOLD, F_ITALIC);

   type Color_Level is range 0 .. 255;

   type Color_Type is record
      Alpha, Red, Green, Blue : Color_Level;
   end record;

   type Style_Type is private;

   type Style_Array is array (Positive range <>) of Style_Type;

   Default_Style : constant Style_Type;

   function To_UString (Value : in String) return UString
     renames Ada.Strings.Unbounded.To_Unbounded_String;

   function To_String (Value : in UString) return String
     renames Ada.Strings.Unbounded.To_String;

   function "=" (Left, Right : in UString) return Boolean
     renames Ada.Strings.Unbounded."=";

   function "=" (Left : in UString; Right : in String) return Boolean
     renames Ada.Strings.Unbounded."=";

   type Printer_Type is limited new Ada.Finalization.Limited_Controlled with private;

   type Printer_Access is access all Printer_Type'Class;

   --  Returns true if the printer is initialized and ready to be used.
   function Is_Open (Printer : in Printer_Type) return Boolean;

   --  Create a new style that can be configured, referenced and used.
   function Create_Style (Printer : in Printer_Type) return Style_Type with
     Pre => Printer.Is_Open;

   function Create_Style (Printer : in Printer_Type;
                          Color   : in Color_Type) return Style_Type with
     Pre => Printer.Is_Open;

   function Create_Style (Printer : in Printer_Type;
                          From    : in Style_Type) return Style_Type with
     Pre => Printer.Is_Open;

   --  Copy the style definition from one style to another.
   procedure Copy_Style (Printer : in Printer_Type;
                         From    : in Style_Type;
                         To      : in Style_Type) with
     Pre => Printer.Is_Open;

   --  Set to use the given color when drawing with this style.
   procedure Set_Color (Printer : in Printer_Type;
                        Style   : in Style_Type;
                        Color   : in Color_Type) with
     Pre => Printer.Is_Open;

   --  Set the style justification.
   procedure Set_Justify (Printer   : in Printer_Type;
                          Style     : in Style_Type;
                          Justify   : in Justify_Type) with
     Pre => Printer.Is_Open;

   --  Set the font style.
   procedure Set_Font (Printer   : in Printer_Type;
                       Style     : in Style_Type;
                       Font      : in Font_Type) with
     Pre => Printer.Is_Open;

   procedure Set_Position (Printer : in out Printer_Type;
                           X       : in X_Type;
                           Y       : in Y_Type);
   procedure Set_Position (Printer  : in out Printer_Type;
                           Position : in Position_Type);

   procedure Set_Style (Printer : in out Printer_Type;
                        Style   : in Style_Type);

   --  Print a message at the current position and with the current
   --  style, then update the position according to the message dimension.
   procedure Put (Printer : in out Printer_Type;
                  Message : in String) with
     Pre => Printer.Is_Open;

   procedure Put (Printer : in out Printer_Type;
                  Box     : in Rect_Type;
                  Style   : in Style_Type;
                  Content : in String) with
     Pre => Printer.Is_Open;

   --  Print the UTF-8 string at the current position.
   procedure Put_UTF8 (Printer : in out Printer_Type;
                       Message : in String) with
     Pre => Printer.Is_Open;

   procedure Put_Wide (Printer : in out Printer_Type;
                       Message : in Wide_Wide_String) with
     Pre => Printer.Is_Open;

   procedure Put_Wide (Printer : in out Printer_Type;
                       Box     : in Rect_Type;
                       Style   : in Style_Type;
                       Content : in Wide_Wide_String) with
     Pre => Printer.Is_Open;

   procedure Put (Printer : in out Printer_Type;
                  Style   : in Style_Type;
                  Content : in String) with
     Pre => Printer.Is_Open;

   procedure Put_UString (Printer : in out Printer_Type;
                          Style   : in Style_Type;
                          Content : in UString) with
     Pre => Printer.Is_Open;

   procedure Put_UString (Printer : in out Printer_Type;
                          Box     : in Rect_Type;
                          Style   : in Style_Type;
                          Content : in UString) with
     Pre => Printer.Is_Open;

   procedure Put_Wide (Printer : in out Printer_Type;
                       Style   : in Style_Type;
                       Content : in Wide_Wide_String) with
     Pre => Printer.Is_Open;

   procedure New_Line (Printer : in out Printer_Type);

   procedure New_Page (Printer : in out Printer_Type);

   --  Get the dimension of the text when it is written with the given style.
   function Get_Dimension (Printer : in Printer_Type;
                           Text    : in String;
                           Style   : in Style_Type) return Dimension_Type;
   function Get_UString_Dimension (Printer : in Printer_Type;
                                   Text    : in UString;
                                   Style   : in Style_Type) return Dimension_Type;
   function Get_Wide_Dimension (Printer : in Printer_Type;
                                Text    : in Wide_Wide_String;
                                Style   : in Style_Type) return Dimension_Type;

private

   type Style_Index_Type is new Natural range 0 .. 255;

   type Style_Type is record
      Index : Style_Index_Type := 0;
   end record;

   Default_Style : constant Style_Type := (Index => 0);

   type Color_Array is array (Positive range <>) of Color_Type;

   type Driver_Type is limited interface;
   type Driver_Access is access all Driver_Type'Class;

   --  Get the drawing area coordinates.
   function Get_Area (Driver : in Driver_Type) return Rect_Type is abstract;

   --  Get the current cliping area coordinates.
   function Get_Clip (Driver : in Driver_Type) return Rect_Type is abstract;

   --  Get the dimension of the text when it is written with the given style.
   function Get_Dimension (Driver : in Driver_Type;
                           Text   : in String;
                           Style  : in Style_Type) return Dimension_Type is abstract;
   function Get_Wide_Dimension (Driver : in Driver_Type;
                                Text   : in Wide_Wide_String;
                                Style  : in Style_Type) return Dimension_Type is abstract;

   function Get_Dimension (Driver   : in Driver_Type;
                           Percent  : in Percent_Type;
                           On_Width : in W_Type := 0) return Dimension_Type is abstract;

   --  Create a new style that can be configured, referenced and used.
   procedure Create_Style (Driver : in out Driver_Type;
                           Style  : out Style_Type) is abstract;

   --  Copy the style definition from one style to another.
   procedure Copy_Style (Driver : in out Driver_Type;
                         From   : in Style_Type;
                         To     : in Style_Type) is abstract;

   --  Set to use the given color when drawing with this style.
   procedure Set_Color (Driver : in out Driver_Type;
                        Style  : in Style_Type;
                        Color  : in Color_Type) is abstract;

   --  Set the style justification.
   procedure Set_Justify (Driver   : in out Driver_Type;
                          Style    : in Style_Type;
                          Justify  : in Justify_Type) is abstract;

   --  Set the font style.
   procedure Set_Font (Driver   : in out Driver_Type;
                       Style    : in Style_Type;
                       Font     : in Font_Type) is abstract;

   --  Set the current cliping area.
   procedure Clip (Driver : in out Driver_Type;
                   Rect   : in Rect_Type) is abstract;

   --  Fill the rectangle by using the given type.
   --  The rectangle is clipped if it intersects the cliping area.
   procedure Fill (Driver : in out Driver_Type;
                   Rect   : in Rect_Type;
                   Style  : in Style_Type) is abstract;

   --  Draw a line starting from (X1, Y1) to (X2, Y2) using the given style.
   --  The line is clipped if it intersects the cliping area.
   procedure Draw (Driver : in out Driver_Type;
                   X1     : in X_Type;
                   Y1     : in Y_Type;
                   X2     : in X_Type;
                   Y2     : in Y_Type;
                   Rect   : in Rect_Type;
                   Style  : in Style_Type) is abstract;

   --  Write the text starting at the top left X, Y corner and using the
   --  given style.  The text is clipped if it intersects the cliping area.
   --  An UTF-8 sequence must be converted to a Wide_Wide_String and
   --  the Put_Wide method must be used.
   procedure Put (Driver  : in out Driver_Type;
                  X       : in X_Type;
                  Y       : in Y_Type;
                  Text    : in String;
                  Style   : in Style_Type;
                  Justify : in Justify_Type := J_DEFAULT) is abstract;
   procedure Put_Wide (Driver  : in out Driver_Type;
                       X       : in X_Type;
                       Y       : in Y_Type;
                       Text    : in Wide_Wide_String;
                       Style   : in Style_Type;
                       Justify : in Justify_Type := J_DEFAULT) is abstract;
   procedure Put_Wide (Driver  : in out Driver_Type;
                       Box     : in Rect_Type;
                       Text    : in Wide_Wide_String;
                       Style   : in Style_Type;
                       Justify : in Justify_Type := J_DEFAULT) is abstract;

   procedure New_Line (Driver : in out Driver_Type) is abstract;

   procedure New_Page (Driver : in out Driver_Type) is abstract;

   type Printer_Type is limited new Ada.Finalization.Limited_Controlled with record
      Pos    : Position_Type;
      Style  : Style_Type;
      Driver : Driver_Access;
   end record;

end PT;
