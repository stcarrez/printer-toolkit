-----------------------------------------------------------------------
--  pt-charts-barcharts -- Printing barcharts
--  Copyright (C) 2021-2024 Stephane Carrez
--  Written by Stephane Carrez (Stephane.Carrez@gmail.com)
--  SPDX-License-Identifier: Apache-2.0
-----------------------------------------------------------------------

package body PT.Charts.Barcharts is

   function Compute_Sum (Values : in Label_Value_Array) return Value_Type is
      Result : Value_Type := Values (Values'First).Value;
   begin
      for I in Values'First + 1 .. Values'Last loop
         Result := Result + Values (I).Value;
      end loop;
      return Result;
   end Compute_Sum;

   --  ------------------------------
   --  Draw an horizontal stacked bar chart with the values in the form:
   --  A first line with the label, a second line with the bars and when
   --  the Convert function is provided a third line with the formatted values.
   --  Example:
   --   1   2    3           4                                     6
   --  +---+----+--------+-----------------------------------+--------------+
   --   132 1320 13020       89320                                 8930
   --  ------------------------------
   procedure Draw_Horizontal_Stacked (Printer : in out Printer_Type;
                                      Rect    : in Rect_Type;
                                      Values  : in Label_Value_Array;
                                      Min     : in Value_Type;
                                      Convert : access function
                                        (Value : in Value_Type) return String) is
      Sum   : constant Value_Type := Compute_Sum (Values);
      Dim   : array (Values'Range) of W_Type;
      Box   : PT.Rect_Type;
      Width : PT.W_Type := Rect.W;
   begin
      --  Step 1: compute min width of text fields.
      for I in Values'Range loop
         Dim (I) := Printer.Get_UString_Dimension (Values (I).Label, Values (I).Style).W;
         if Convert /= null then
            declare
               W : constant W_Type
                  := Printer.Get_Dimension (Convert (Values (I).Value), Values (I).Style).W;
            begin
               if W > Dim (I) then
                  Dim (I) := W;
               end if;
            end;
         end if;
         Width := Width - Dim (I);
      end loop;

      --  Step 2: make the width of bars proportional
      for I in Values'Range loop
         Dim (I) := Dim (I) + Percent (Width, Values (I).Value, Min, Sum);
      end loop;

      Printer.Driver.Clip (Rect);
      Printer.Pos.X := Rect.X;
      Printer.Pos.Y := Rect.Y;
      Box.X := Rect.X;
      Box.Y := Rect.Y;
      Box.H := Rect.H;
      for I in Values'Range loop
         Box.W := Dim (I);
         Printer.Pos.X := Box.X;
         Printer.Set_Justify (Values (I).Style, J_CENTER);
         Printer.Put_UString (Box, Values (I).Style, Values (I).Label);
         Box.X := Box.X + Box.W;
      end loop;
      Printer.Driver.New_Line;

      Printer.Driver.Clip (Rect);
      Box.X := Rect.X;
      Box.Y := Rect.Y;
      Box.H := Rect.H;
      for I in Values'Range loop
         Box.W := Dim (I);
         Printer.Driver.Fill (Box, Values (I).Style);
         Box.X := Box.X + Box.W;
      end loop;

      if Convert /= null then
         Printer.Driver.New_Line;

         Printer.Driver.Clip (Rect);
         Printer.Pos.X := Rect.X;
         Printer.Pos.Y := Rect.Y;
         Box.X := Rect.X;
         Box.Y := Rect.Y;
         Box.H := Rect.H;
         for I in Values'Range loop
            Box.W := Dim (I);
            Printer.Pos.X := Box.X;
            Printer.Put (Box, Values (I).Style, Convert (Values (I).Value));
            Box.X := Box.X + Box.W;
         end loop;
      end if;

   end Draw_Horizontal_Stacked;

   --  ------------------------------
   --  Draw a progress bar in the given rectangle to show the position
   --  of the value within the min/max value range.
   --  ------------------------------
   procedure Draw_Progress (Printer : in out PT.Printer_Type'Class;
                            Rect    : in Rect_Type;
                            Value   : in Value_Type;
                            Min     : in Value_Type;
                            Max     : in Value_Type;
                            Style1  : in Style_Type;
                            Style2  : in Style_Type) is
      W : constant W_Type := Percent (Rect.W, Value, Min, Max);
   begin
      PT.Charts.Fill (Printer, Rect, W, Style1, Style2);
      Printer.Pos.X := Printer.Pos.X + Rect.W;
   end Draw_Progress;

   --  ------------------------------
   --  Draw a progress bar in the given rectangle to show the position
   --  of the value within the min/max value range.
   --  ------------------------------
   procedure Draw_Bar (Printer : in out PT.Printer_Type'Class;
                       Rect    : in Rect_Type;
                       Value   : in Value_Type;
                       Min     : in Value_Type;
                       Max     : in Value_Type;
                       Style   : in Style_Type;
                       Width   : out W_Type) is
   begin
      Width := Percent (Rect.W, Value, Min, Max);
      declare
         R1 : constant Rect_Type := (X => Rect.X, Y => Rect.Y,
                                     W => Width, H => Rect.H);
      begin
         Printer.Driver.Clip (Rect);
         Printer.Driver.Fill (R1, Style);
         Printer.Pos.X := Rect.X + Width;
      end;
   end Draw_Bar;

end PT.Charts.Barcharts;
