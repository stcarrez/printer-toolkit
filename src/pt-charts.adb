-----------------------------------------------------------------------
--  pt-charts -- Printing charts
--  Copyright (C) 2021 Stephane Carrez
--  Written by Stephane Carrez (Stephane.Carrez@gmail.com)
--  SPDX-License-Identifier: Apache-2.0
-----------------------------------------------------------------------

package body PT.Charts is

   --  Create the chart printer and configure it with the given width.
   function Create (From  : in PT.Printer_Type'Class;
                    Width : in W_Type := 0) return Printer_Type is
   begin
      return Printer : Printer_Type do
         Printer.Driver := From.Driver;
      end return;
   end Create;

   procedure Draw_Barchart (Printer     : in out Printer_Type;
                            Label_Style : in Style_Type;
                            Samples     : in Sample_Map) is
      Name_Length : Natural := 0;
      Max_Length  : Natural := 0;
      Max_Sum     : Sample_Value := 0;
      Label_W     : W_Type;
   begin
      --  Step 1: find out dimension and bounds of values.
      for Pos in Samples.Iterate loop
         declare
            Name   : constant String := Sample_Maps.Key (Pos);
            Values : constant Sample_Data := Sample_Maps.Element (Pos);
            Sum    : Sample_Value := 0;
         begin
            if Name_Length < Name'Length then
               Name_Length := Name'Length;
            end if;
            if Max_Length < Values.Len then
               Max_Length := Values.Len;
            end if;
            for Val of Values.Values loop
               Sum := Sum + Val;
            end loop;
            if Sum > Max_Sum then
               Max_Sum := Sum;
            end if;
         end;
      end loop;

      Printer.Pos.X := 1;
      Printer.Pos.Y := 1;
      Label_W := W_Type (Name_Length);

      --  Step 2: write the chart.
      for Pos in Samples.Iterate loop
         declare
            Name   : constant String := Sample_Maps.Key (Pos);
            Values : constant Sample_Data := Sample_Maps.Element (Pos);
            Rect   : Rect_Type;
         begin
            Rect.X := Printer.Pos.X;
            Rect.Y := Printer.Pos.Y;
            Rect.W := Label_W;
            Rect.H := 1;
            Printer.Driver.Clip (Rect);
            Printer.Driver.Put (Printer.Pos.X, Printer.Pos.Y, Name, Label_Style);

            Printer.Pos.X := Printer.Pos.X + Rect.W;

            Rect.X := Printer.Pos.X;
            Rect.W := 80 - Label_W;
            Rect.H := 1;
            Printer.Driver.Clip (Rect);

            for I in 1 .. Values.Len loop
               Rect.X := Printer.Pos.X;
               Rect.Y := Printer.Pos.Y;
               Rect.W := W_Type (Values.Values (I));
               Rect.H := 1;
               Printer.Driver.Fill (Rect, Values.Styles (I));
               Printer.Pos.X := Printer.Pos.X + Rect.W;
            end loop;
            Printer.Driver.New_Line;
            Printer.Pos.X := 1;
            Printer.Pos.Y := 1;
         end;
      end loop;
   end Draw_Barchart;

   --  ------------------------------
   --  Fill the rectangle [Rect.X, Rect.Y, Width, Rect.W] with the Style1
   --  color and the rest of the area with the Style2.
   --  ------------------------------
   procedure Fill (Printer : in out PT.Printer_Type'Class;
                   Rect    : in Rect_Type;
                   Width   : in W_Type;
                   Style1  : in Style_Type;
                   Style2  : in Style_Type) is
      R1 : constant Rect_Type := (X => Rect.X, Y => Rect.Y,
                                  W => Width, H => Rect.H);
   begin
      Printer.Driver.Clip (Rect);
      Printer.Driver.Fill (R1, Style1);
      if Width < Rect.W then
         declare
            R2 : constant Rect_Type := (X => Rect.X + Width,
                                        Y => Rect.Y,
                                        W => Rect.W - Width,
                                        H => Rect.H);
         begin
            Printer.Driver.Fill (R2, Style2);
         end;
      end if;
   end Fill;

   --  ------------------------------
   --  Draw a progress bar in the given rectangle to show the position
   --  of the value within the min/max value range.
   --  ------------------------------
   procedure Draw_Bar (Printer : in out Printer_Type;
                       Rect    : in Rect_Type;
                       Value   : in Value_Type;
                       Min     : in Value_Type := Value_Type'First;
                       Max     : in Value_Type := Value_Type'Last;
                       Style1  : in Style_Type;
                       Style2  : in Style_Type) is
      D : constant W_Type := Value - Min;
      W : constant W_Type := (Rect.W * D) / (Max - Min);
   begin
      Fill (Printer, Rect, W, Style1, Style2);
   end Draw_Bar;

   function Width_Fixed (Width : in W_Type;
                         Value, Min, Max : in Value_Type) return W_Type is
      D : constant Float := Float (Width) * Float (Value_Type (Value - Min));
   begin
      return W_Type (D / Float (Max - Min));
   end Width_Fixed;

   function Width_Range (Width : in W_Type;
                         Value, Min, Max : in Value_Type) return W_Type is
      D : constant Long_Integer
        := Long_Integer (Width) * Long_Integer (Value_Type (Value - Min));
   begin
      return W_Type (D / Long_Integer (Max - Min));
   end Width_Range;

end PT.Charts;
