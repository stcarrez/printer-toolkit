-----------------------------------------------------------------------
--  pt-texts -- Printing text fields
--  Copyright (C) 2021, 2024 Stephane Carrez
--  Written by Stephane Carrez (Stephane.Carrez@gmail.com)
--  SPDX-License-Identifier: Apache-2.0
-----------------------------------------------------------------------

package PT.Texts with Preelaborate is

   type Field_Type is private;

   type Field_Array is array (Positive range <>) of Field_Type;

   type Printer_Type is limited new PT.Printer_Type with private;

   --  Create the text printer.
   function Create (From : in PT.Printer_Type'Class) return Printer_Type with
     Pre => From.Is_Open;

   --  Create a new field with the given style and width.
   procedure Create_Field (Printer : in out Printer_Type;
                           Field   : out Field_Type;
                           Style   : in Style_Type;
                           Percent : in Percent_Type) with
     Pre => Printer.Is_Open;

   procedure Create_Field (Printer   : in out Printer_Type;
                           Field     : out Field_Type;
                           Style     : in Style_Type;
                           Dimension : in Dimension_Type) with
     Pre => Printer.Is_Open;

   --  Set the style to be used for the field.
   procedure Set_Style (Printer : in Printer_Type;
                        Field   : in out Field_Type;
                        Style   : in Style_Type) with
     Pre => Printer.Is_Open;

   --  Set the max dimension that this field is expected to have.
   procedure Set_Max_Dimension (Printer   : in out Printer_Type;
                                Field     : in out Field_Type;
                                Dimension : in Dimension_Type) with
     Pre => Printer.Is_Open;

   --  Set the top left padding for the field.
   procedure Set_Top_Left_Padding (Printer   : in out Printer_Type;
                                   Field     : in out Field_Type;
                                   Dimension : in Dimension_Type) with
     Pre => Printer.Is_Open;

   --  Set the bottom right padding for the field.
   procedure Set_Bottom_Right_Padding (Printer   : in out Printer_Type;
                                       Field     : in out Field_Type;
                                       Dimension : in Dimension_Type) with
     Pre => Printer.Is_Open;

   --  Set the field justification.
   procedure Set_Justify (Printer   : in out Printer_Type;
                          Field     : in out Field_Type;
                          Justify   : in Justify_Type) with
     Pre => Printer.Is_Open;

   --  Layout the fields according to the printing area.
   procedure Layout_Fields (Printer   : in out Printer_Type;
                            Fields    : in out Field_Array;
                            Max_Width : in W_Type := 0) with
     Pre => Printer.Is_Open;

   --  Get the computed dimension of the field when it is printed.
   function Get_Dimension (Printer : in Printer_Type;
                           Field   : in Field_Type) return Dimension_Type with
     Pre => Printer.Is_Open;

   function Get_Dimension (Printer : in Printer_Type;
                           Field   : in Field_Type;
                           Content : in String) return Dimension_Type with
     Pre => Printer.Is_Open;

   --  Get the field bounding box rectangle.
   function Get_Box (Printer : in Printer_Type;
                     Field : in Field_Type) return Rect_Type with
     Pre => Printer.Is_Open;

   procedure Put (Printer : in out Printer_Type;
                  Field   : in Field_Type;
                  Content : in String) with
     Pre => Printer.Is_Open;

   procedure Put_UString (Printer : in out Printer_Type;
                          Field   : in Field_Type;
                          Content : in UString) with
     Pre => Printer.Is_Open;

   procedure Put_Wide (Printer : in out Printer_Type;
                       Field   : in Field_Type;
                       Content : in Wide_Wide_String) with
     Pre => Printer.Is_Open;

   procedure Put_Int (Printer : in out Printer_Type;
                      Field   : in Field_Type;
                      Value   : in Integer) with
     Pre => Printer.Is_Open;

   procedure Put_Long (Printer : in out Printer_Type;
                       Field   : in Field_Type;
                       Value   : in Long_Long_Integer) with
     Pre => Printer.Is_Open;

   --  Update the field to try reserving room when writing the content.
   procedure Will_Put (Printer : in out Printer_Type;
                       Field   : in out Field_Type;
                       Content : in String) with
     Pre => Printer.Is_Open;

   procedure Set_Position (Printer : in out Printer_Type;
                           Field   : in Field_Type);

private

   type Field_Type is record
      Pos        : Position_Type;
      Percent    : Percent_Type := 10.0;
      Computed   : Percent_Type := 0.0;
      Style      : Style_Type := Default_Style;
      Justify    : Justify_Type := J_LEFT;
      Pad_Left   : Dimension_Type;
      Pad_Right  : Dimension_Type;
      Dimension  : Dimension_Type;
      Max_Dim    : Dimension_Type;
   end record;

   type Printer_Type is limited new PT.Printer_Type with record
      Column_Count : Natural := 0;
   end record;

end PT.Texts;
