-----------------------------------------------------------------------
--  pt-texts -- Printing text fields
--  Copyright (C) 2021, 2024 Stephane Carrez
--  Written by Stephane Carrez (Stephane.Carrez@gmail.com)
--  SPDX-License-Identifier: Apache-2.0
-----------------------------------------------------------------------

package body PT.Texts is

   --  ------------------------------
   --  Create the text printer.
   --  ------------------------------
   function Create (From : in PT.Printer_Type'Class) return Printer_Type is
   begin
      return Result : Printer_Type do
         Result.Driver := From.Driver;
         Result.Pos.X := 1;
         Result.Pos.Y := 1;
      end return;
   end Create;

   --  ------------------------------
   --  Create a new field with the given style and width.
   --  ------------------------------
   procedure Create_Field (Printer : in out Printer_Type;
                           Field   : out Field_Type;
                           Style   : in Style_Type;
                           Percent : in Percent_Type) is
   begin
      Field := (Percent  => Percent,
                Style    => Style,
                Computed => Percent,
                others   => <>);
   end Create_Field;

   procedure Create_Field (Printer   : in out Printer_Type;
                           Field     : out Field_Type;
                           Style     : in Style_Type;
                           Dimension : in Dimension_Type) is
   begin
      Field := (Percent  => 0.0,
                Style    => Style,
                Computed => 0.0,
                Max_Dim  => Dimension,
                Dimension => Dimension,
                others   => <>);
   end Create_Field;

   --  ------------------------------
   --  Set the style to be used for the field.
   --  ------------------------------
   procedure Set_Style (Printer : in Printer_Type;
                        Field   : in out Field_Type;
                        Style   : in Style_Type) is
   begin
      Field.Style := Style;
   end Set_Style;

   --  ------------------------------
   --  Set the max dimension that this field is expected to have.
   --  ------------------------------
   procedure Set_Max_Dimension (Printer   : in out Printer_Type;
                                Field     : in out Field_Type;
                                Dimension : in Dimension_Type) is
   begin
      Field.Max_Dim := Dimension;
   end Set_Max_Dimension;

   --  ------------------------------
   --  Set the top left padding for the field.
   --  ------------------------------
   procedure Set_Top_Left_Padding (Printer   : in out Printer_Type;
                                   Field     : in out Field_Type;
                                   Dimension : in Dimension_Type) is
   begin
      Field.Pad_Left := Dimension;
   end Set_Top_Left_Padding;

   --  ------------------------------
   --  Set the bottom right padding for the field.
   --  ------------------------------
   procedure Set_Bottom_Right_Padding (Printer   : in out Printer_Type;
                                       Field     : in out Field_Type;
                                       Dimension : in Dimension_Type) is
   begin
      Field.Pad_Right := Dimension;
   end Set_Bottom_Right_Padding;

   --  ------------------------------
   --  Set the field justification.
   --  ------------------------------
   procedure Set_Justify (Printer   : in out Printer_Type;
                          Field     : in out Field_Type;
                          Justify   : in Justify_Type) is
   begin
      Field.Justify := Justify;
   end Set_Justify;

   --  ------------------------------
   --  Update the field to try reserving room when writing the content.
   --  ------------------------------
   procedure Will_Put (Printer : in out Printer_Type;
                       Field   : in out Field_Type;
                       Content : in String) is
      Dim : constant Dimension_Type := Printer.Driver.Get_Dimension (Content, Field.Style);
   begin
      if Dim.W > Field.Max_Dim.W then
         Field.Max_Dim.W := Dim.W;
      end if;
      if Dim.H > Field.Max_Dim.H then
         Field.Max_Dim.H := Dim.H;
      end if;
   end Will_Put;

   --  ------------------------------
   --  Layout the fields according to the printing area.
   --  ------------------------------
   procedure Layout_Fields (Printer   : in out Printer_Type;
                            Fields    : in out Field_Array;
                            Max_Width : in W_Type := 0) is
      Area      : constant Rect_Type := Printer.Driver.Get_Area;
      Area_Max  : constant W_Type := (if Max_Width = 0 then Area.W else Max_Width);
      Sum       : Float := 0.0;
      Sum_Dim   : Dimension_Type := (0, 0);
      DW        : Integer;
      Pct_Count : Natural := 0;
      Def_Count : Natural := 0;
      Pos       : Position_Type := (1, 1);
   begin
      for Field of Fields loop
         Field.Pos := Pos;
         Sum := Sum + Float (Field.Percent);
         if Field.Percent = 0.0 then
            Field.Dimension := Field.Max_Dim;
            if Field.Max_Dim = (0, 0) then
               Def_Count := Def_Count + 1;
            end if;
         else
            Field.Dimension := Printer.Driver.Get_Dimension (Field.Percent,
                                                             Max_Width);
            Pct_Count := Pct_Count + 1;
         end if;
         Sum_Dim := Sum_Dim + Field.Dimension;
         if Pos.X /= 0 and then Field.Dimension.W > 0 then
            Pos.X := Pos.X + Field.Dimension.W + Field.Pad_Left.W + Field.Pad_Right.W;
         else
            Pos.X := 0;
         end if;
      end loop;

      DW := Integer (Area_Max) - Integer (Sum_Dim.W);
      if DW > 0 and Pct_Count > 0 and Def_Count = 0 then
         Pos := (1, 1);
         for Field of Fields loop
            Field.Pos := Pos;
            if Field.Percent /= 0.0 then
               if DW > 0 then
                  Field.Dimension.W := Field.Dimension.W + W_Type (DW / Pct_Count);
               else
                  Field.Dimension.W := Field.Dimension.W - W_Type (-(DW / Pct_Count));
               end if;
            end if;
            if Pos.X /= 0 and then Field.Dimension.W > 0 then
               Pos.X := Pos.X + Field.Dimension.W + Field.Pad_Left.W + Field.Pad_Right.W;
            else
               Pos.X := 0;
            end if;
         end loop;
      end if;

   end Layout_Fields;

   --  ------------------------------
   --  Get the computed dimension of the field when it is printed.
   --  ------------------------------
   function Get_Dimension (Printer : in Printer_Type;
                           Field   : in Field_Type) return Dimension_Type is
   begin
      if Field.Dimension.W > 0 then
         return Field.Dimension;
      elsif Field.Percent /= 0.0 then
         return Printer.Driver.Get_Dimension (Field.Percent);
      else
         return Field.Max_Dim;
      end if;
   end Get_Dimension;

   function Get_Dimension (Printer : in Printer_Type;
                           Field   : in Field_Type;
                           Content : in String) return Dimension_Type is
   begin
      if Field.Dimension.W > 0 then
         return Field.Dimension;
      elsif Field.Percent /= 0.0 then
         return Printer.Driver.Get_Dimension (Field.Percent);
      elsif Field.Max_Dim /= (0, 0) then
         return Field.Max_Dim;
      else
         return Printer.Driver.Get_Dimension (Content, Field.Style);
      end if;
   end Get_Dimension;

   --  ------------------------------
   --  Get the field bounding box rectangle.
   --  ------------------------------
   function Get_Box (Printer : in Printer_Type;
                     Field : in Field_Type) return Rect_Type is
      D : constant Dimension_Type := Printer.Get_Dimension (Field);
   begin
      return (X => Printer.Pos.X + Field.Pad_Left.W,
              Y => Printer.Pos.Y + Field.Pad_Left.H,
              W => D.W,
              H => D.H);
   end Get_Box;

   procedure Put (Printer : in out Printer_Type;
                  Field   : in Field_Type;
                  Content : in String) is
      Dim  : constant Dimension_Type := Printer.Get_Dimension (Field, Content);
      Rect : Rect_Type;
   begin
      if Field.Pos.X /= 0 and then Field.Pos.Y /= 0 then
         Printer.Set_Position (Field.Pos);
      end if;
      Rect.X := Printer.Pos.X + Field.Pad_Left.W;
      Rect.Y := Printer.Pos.Y + Field.Pad_Left.H;
      Rect.W := Dim.W;
      Rect.H := Dim.H;
      Printer.Driver.Clip (Rect);
      Printer.Driver.Put_Wide (Box     => Rect,
                               Text    => To_WString (Content),
                               Style   => Field.Style,
                               Justify => Field.Justify);
      Printer.Pos.X := Rect.X + Rect.W + Field.Pad_Right.W;
   end Put;

   procedure Put_UString (Printer : in out Printer_Type;
                          Field   : in Field_Type;
                          Content : in UString) is
   begin
      Printer.Put (Field, To_String (Content));
   end Put_UString;

   procedure Put_Wide (Printer : in out Printer_Type;
                       Field   : in Field_Type;
                       Content : in Wide_Wide_String) is
   begin
      null;
   end Put_Wide;

   procedure Put_Int (Printer : in out Printer_Type;
                      Field   : in Field_Type;
                      Value   : in Integer) is
      Image : constant String := Integer'Image (Value);
   begin
      if Image (Image'First) = ' ' then
         Printer.Put (Field, Image (Image'First + 1 .. Image'Last));
      else
         Printer.Put (Field, Image);
      end if;
   end Put_Int;

   procedure Put_Long (Printer : in out Printer_Type;
                       Field   : in Field_Type;
                       Value   : in Long_Long_Integer) is
      Image : constant String := Long_Long_Integer'Image (Value);
   begin
      if Image (Image'First) = ' ' then
         Printer.Put (Field, Image (Image'First + 1 .. Image'Last));
      else
         Printer.Put (Field, Image);
      end if;
   end Put_Long;

   procedure Set_Position (Printer : in out Printer_Type;
                           Field   : in Field_Type) is
   begin
      Printer.Set_Position (Field.Pos);
   end Set_Position;

end PT.Texts;
