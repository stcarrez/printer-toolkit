-----------------------------------------------------------------------
--  pt-colors -- Some pre-defined colors
--  Copyright (C) 2023, 2024 Stephane Carrez
--  Written by Stephane Carrez (Stephane.Carrez@gmail.com)
--  SPDX-License-Identifier: Apache-2.0
-----------------------------------------------------------------------

package body PT.Colors is

   type Chroma is delta 0.001 range 0.0 .. 255.0;

   --  ------------------------------
   --  Convert HSV color to RVB.
   --  ------------------------------
   function To_Color (H : Hue; S : Saturation; V : Value) return Color_Type is
      Hq : constant Hue := (if H = 360.0 then 0.0 else H / 60.0);
      Hr : constant Natural := Natural (Hq);
      Vc : constant Chroma := Chroma (255.0) * Chroma (V);
      F  : constant Chroma := (if Hue (Hr) > Hq then Chroma (Hq - Hue (Hr - 1))
                               else Chroma (Hq - Hue (Hr)));
      P  : constant Chroma := Vc * (Chroma (1.0) - Chroma (S));
      Q  : constant Chroma := Vc * (Chroma (1.0) - Chroma (S) * F);
      T  : constant Chroma := Vc * (Chroma (1.0) - Chroma (S) * (1.0 - F));
   begin
      case Hr is
         when 0 =>
            return (Alpha => Color_Level'Last,
                    Red   => Color_Level (Vc),
                    Green => Color_Level (T),
                    Blue  => Color_Level (P));
         when 1 =>
            return (Alpha => Color_Level'Last,
                    Red   => Color_Level (Q),
                    Green => Color_Level (Vc),
                    Blue  => Color_Level (P));
         when 2 =>
            return (Alpha => Color_Level'Last,
                    Red   => Color_Level (P),
                    Green => Color_Level (Vc),
                    Blue  => Color_Level (T));
         when 3 =>
            return (Alpha => Color_Level'Last,
                    Red   => Color_Level (P),
                    Green => Color_Level (Q),
                    Blue  => Color_Level (Vc));
         when others =>
            return (Alpha => Color_Level'Last,
                    Red   => Color_Level (Vc),
                    Green => Color_Level (P),
                    Blue  => Color_Level (Q));
      end case;
   end To_Color;

   Conversion : constant String (1 .. 16) := "0123456789ABCDEF";

   function To_Hex_Low (C : in Color_Level) return Character is
     (Conversion (1 + Natural (C mod 16)));

   function To_Hex_High (C : in Color_Level) return Character is
     (Conversion (1 + Natural (C / 16)));

   --  ------------------------------
   --  Convert the color to the hexadecimal string value in the form: <rrggbb>
   --  ------------------------------
   function To_Hex (C : Color_Type) return String is
      Result : String (1 .. 6);
   begin
      Result (1) := To_Hex_High (C.Red);
      Result (2) := To_Hex_Low (C.Red);
      Result (3) := To_Hex_High (C.Green);
      Result (4) := To_Hex_Low (C.Green);
      Result (5) := To_Hex_High (C.Blue);
      Result (6) := To_Hex_Low (C.Blue);
      return Result;
   end To_Hex;

end PT.Colors;
