-----------------------------------------------------------------------
--  pt-drivers-texts -- Printer driver
--  Copyright (C) 2021, 2023, 2024 Stephane Carrez
--  Written by Stephane Carrez (Stephane.Carrez@gmail.com)
--  SPDX-License-Identifier: Apache-2.0
-----------------------------------------------------------------------
with Ada.Unchecked_Deallocation;

package body PT.Drivers.Texts is

   function To_Foreground (Color : in PT.Color_Type) return String;
   function To_Background (Color : in PT.Color_Type) return String;

   function Get_Cols return Integer with
     Import => True, Convention => C, Link_Name => "_pt_get_cols";

   function Get_Rows return Integer with
     Import => True, Convention => C, Link_Name => "_pt_get_rows";

   function Screen_Dimension return Dimension_Type is
      Cols : constant Integer := Get_Cols;
      Rows : constant Integer := Get_Rows;
   begin
      return (W => (if Cols <= 0 then 80 else W_Type (Cols)),
              H => (if Rows <= 0 then 24 else H_Type (Rows)));
   end Screen_Dimension;

   --  ------------------------------
   --  Convert the foreground color to a suitable ANSI sequence.
   --  ------------------------------
   function To_Foreground (Color : in PT.Color_Type) return String is
   begin
      if Color = PT.Colors.Black then
         return AnsiAda.Foreground (AnsiAda.Black);
      end if;
      if Color = PT.Colors.Light_Red then
         return AnsiAda.Foreground (AnsiAda.Light_Red);
      end if;
      if Color = PT.Colors.Light_Green then
         return AnsiAda.Foreground (AnsiAda.Light_Green);
      end if;
      if Color = PT.Colors.Light_Yellow then
         return AnsiAda.Foreground (AnsiAda.Light_Yellow);
      end if;
      if Color = PT.Colors.Light_Blue then
         return AnsiAda.Foreground (AnsiAda.Light_Blue);
      end if;
      if Color = PT.Colors.Light_Magenta then
         return AnsiAda.Foreground (AnsiAda.Light_Magenta);
      end if;
      if Color = PT.Colors.Light_Cyan then
         return AnsiAda.Foreground (AnsiAda.Light_Cyan);
      end if;
      if Color = PT.Colors.Red then
         return AnsiAda.Foreground (AnsiAda.Red);
      end if;
      if Color = PT.Colors.Green then
         return AnsiAda.Foreground (AnsiAda.Green);
      end if;
      if Color = PT.Colors.Yellow then
         return AnsiAda.Foreground (AnsiAda.Yellow);
      end if;
      if Color = PT.Colors.Blue then
         return AnsiAda.Foreground (AnsiAda.Blue);
      end if;
      if Color = PT.Colors.Magenta then
         return AnsiAda.Foreground (AnsiAda.Magenta);
      end if;
      if Color = PT.Colors.Cyan then
         return AnsiAda.Foreground (AnsiAda.Cyan);
      end if;
      return AnsiAda.Foreground (R => AnsiAda.True_RGB (Color.Red),
                                 G => AnsiAda.True_RGB (Color.Green),
                                 B => AnsiAda.True_RGB (Color.Blue));
   end To_Foreground;

   --  ------------------------------
   --  Convert the background color to a suitable ANSI sequence.
   --  ------------------------------
   function To_Background (Color : in PT.Color_Type) return String is
   begin
      if Color = PT.Colors.Black then
         return AnsiAda.Background (AnsiAda.Black);
      end if;
      if Color = PT.Colors.Light_Red then
         return AnsiAda.Background (AnsiAda.Light_Red);
      end if;
      if Color = PT.Colors.Light_Green then
         return AnsiAda.Background (AnsiAda.Light_Green);
      end if;
      if Color = PT.Colors.Light_Yellow then
         return AnsiAda.Background (AnsiAda.Light_Yellow);
      end if;
      if Color = PT.Colors.Light_Blue then
         return AnsiAda.Background (AnsiAda.Light_Blue);
      end if;
      if Color = PT.Colors.Light_Magenta then
         return AnsiAda.Background (AnsiAda.Light_Magenta);
      end if;
      if Color = PT.Colors.Light_Cyan then
         return AnsiAda.Background (AnsiAda.Light_Cyan);
      end if;
      if Color = PT.Colors.Red then
         return AnsiAda.Background (AnsiAda.Red);
      end if;
      if Color = PT.Colors.Green then
         return AnsiAda.Background (AnsiAda.Green);
      end if;
      if Color = PT.Colors.Yellow then
         return AnsiAda.Background (AnsiAda.Yellow);
      end if;
      if Color = PT.Colors.Blue then
         return AnsiAda.Background (AnsiAda.Blue);
      end if;
      if Color = PT.Colors.Magenta then
         return AnsiAda.Background (AnsiAda.Magenta);
      end if;
      if Color = PT.Colors.Cyan then
         return AnsiAda.Background (AnsiAda.Cyan);
      end if;
      return AnsiAda.Background (R => AnsiAda.True_RGB (Color.Red),
                                 G => AnsiAda.True_RGB (Color.Green),
                                 B => AnsiAda.True_RGB (Color.Blue));
   end To_Background;

   function Create (Width     : in X_Type := 80;
                    Max_Width : in X_Type := 0) return Printer_Type is
   begin
      return Printer : Printer_Type do
         Printer.Text_Driver := new Driver_Type (Cols => Max (Width, Max_Width),
                                                 Rows => 1);
         Printer.Text_Driver.Printer := Printer'Unchecked_Access;
         Printer.Text_Driver.Flush := Printer.Flush;
         Printer.Text_Driver.Clear;
         Printer.Text_Driver.Area.W := Width;
         Printer.Driver := Printer.Text_Driver.all'Access;
      end return;
   end Create;

   function Create (Dimension : in Dimension_Type;
                    Max_Width : in X_Type := 0) return Printer_Type is
   begin
      return Create (Dimension.W, Max_Width);
   end Create;

   procedure Create (Printer   : in out Printer_Type;
                     Width     : in X_Type;
                     Max_Width : in X_Type := 0) is
   begin
      Printer.Text_Driver := new Driver_Type (Cols => Max (Width, Max_Width),
                                              Rows => 1);
      Printer.Text_Driver.Printer := Printer'Unchecked_Access;
      Printer.Text_Driver.Flush := Printer.Flush;
      Printer.Text_Driver.Clear;
      Printer.Text_Driver.Area.W := Width;
      Printer.Driver := Printer.Text_Driver.all'Access;
   end Create;

   overriding
   procedure Initialize (Driver : in out Printer_Type) is
   begin
      null;
   end Initialize;

   overriding
   procedure Finalize (Driver : in out Printer_Type) is
      procedure Free is
         new Ada.Unchecked_Deallocation (Object => Driver_Type'Class,
                                         Name   => Driver_Access);
   begin
      Free (Driver.Text_Driver);
   end Finalize;

   --  ------------------------------
   --  Set the procedure which will be called to flush the printer's content.
   --  ------------------------------
   procedure Set_Flush (Printer : in out Printer_Type;
                        Flush   : in Flush_Procedure) is
   begin
      Printer.Flush := Flush;
      if Printer.Text_Driver /= null then
         Printer.Text_Driver.Flush := Flush;
      end if;
   end Set_Flush;

   --  Release the style when it is no longer used.
   procedure Delete_Style (Printer : in out Printer_Type;
                           Style   : in out Style_Type) is
   begin
      null;
   end Delete_Style;

   --  ------------------------------
   --  Set the character fill style.
   --  ------------------------------
   procedure Set_Fill (Printer : in out Printer_Type;
                       Style   : in Style_Type;
                       Fill    : in Wide_Wide_Character) is
   begin
      Printer.Text_Driver.Styles (Style.Index).Fill := Fill;
   end Set_Fill;

   --  ------------------------------
   --  Set to use the given background color when drawing with this style.
   --  ------------------------------
   procedure Set_Background (Printer : in out Printer_Type;
                             Style   : in Style_Type;
                             Color   : in Color_Type) is
   begin
      Printer.Text_Driver.Styles (Style.Index).Background := Color;
   end Set_Background;

   --  ------------------------------
   --  Get the drawing area coordinates.
   --  ------------------------------
   overriding
   function Get_Area (Driver : in Driver_Type) return Rect_Type is
   begin
      return Driver.Area;
   end Get_Area;

   --  ------------------------------
   --  Get the current cliping area coordinates.
   --  ------------------------------
   overriding
   function Get_Clip (Driver : in Driver_Type) return Rect_Type is
   begin
      return Driver.Clip;
   end Get_Clip;

   --  ------------------------------
   --  Get the dimension of the text when it is written with the given style.
   --  ------------------------------
   overriding
   function Get_Dimension (Driver : in Driver_Type;
                           Text   : in String;
                           Style  : in Style_Type) return Dimension_Type is
      Result : Dimension_Type;
   begin
      Result.H := 1;
      Result.W := Text'Length;
      return Result;
   end Get_Dimension;

   overriding
   function Get_Wide_Dimension (Driver : in Driver_Type;
                                Text   : in Wide_Wide_String;
                                Style  : in Style_Type) return Dimension_Type is
      Result : Dimension_Type;
   begin
      Result.H := 1;
      Result.W := Text'Length;
      return Result;
   end Get_Wide_Dimension;

   overriding
   function Get_Dimension (Driver   : in Driver_Type;
                           Percent  : in Percent_Type;
                           On_Width : in W_Type := 0) return Dimension_Type is
      Result : Dimension_Type;
   begin
      Result.H := 1;
      if On_Width = 0 or else On_Width > Driver.Area.W then
         Result.W := W_Type (Float (Driver.Area.W) * Float (Percent) / 100.0);
      else
         Result.W := W_Type (Float (On_Width) * Float (Percent) / 100.0);
      end if;
      return Result;
   end Get_Dimension;

   --  ------------------------------
   --  Create a new style that can be configured, referenced and used.
   --  ------------------------------
   overriding
   procedure Create_Style (Driver : in out Driver_Type;
                           Style  : out Style_Type) is
   begin
      for I in Driver.Styles'Range loop
         if not Driver.Styles (I).Allocated then
            Driver.Styles (I).Allocated := True;
            Style := (Index => I);
            return;
         end if;
      end loop;
   end Create_Style;

   --  ------------------------------
   --  Copy the style definition from one style to another.
   --  ------------------------------
   overriding
   procedure Copy_Style (Driver : in out Driver_Type;
                         From   : in Style_Type;
                         To     : in Style_Type) is
   begin
      Driver.Styles (To.Index) := Driver.Styles (From.Index);
   end Copy_Style;

   --  ------------------------------
   --  Set to use the given color when drawing with this style.
   --  ------------------------------
   overriding
   procedure Set_Color (Driver : in out Driver_Type;
                        Style  : in Style_Type;
                        Color  : in Color_Type) is
   begin
      Driver.Styles (Style.Index).Color := Color;
   end Set_Color;

   --  ------------------------------
   --  Set the style justification.
   --  ------------------------------
   overriding
   procedure Set_Justify (Driver   : in out Driver_Type;
                          Style    : in Style_Type;
                          Justify  : in Justify_Type) is
   begin
      Driver.Styles (Style.Index).Justify := Justify;
   end Set_Justify;

   --  ------------------------------
   --  Set the font style.
   --  ------------------------------
   overriding
   procedure Set_Font (Driver   : in out Driver_Type;
                       Style    : in Style_Type;
                       Font     : in Font_Type) is
   begin
      Driver.Styles (Style.Index).Font := Font;
   end Set_Font;

   --  ------------------------------
   --  Set the current cliping area.
   --  ------------------------------
   overriding
   procedure Clip (Driver : in out Driver_Type;
                   Rect   : in Rect_Type) is
   begin
      Driver.Clip := Rect;
      if Driver.Clip.X > Driver.Line.Cols then
         Driver.Clip.X := Driver.Line.Cols;
      end if;
      if Driver.Clip.X + Driver.Clip.W > Driver.Line.Cols then
         Driver.Clip.W := Driver.Line.Cols - Driver.Clip.X;
      end if;
   end Clip;

   --  Fill the rectangle by using the given type.
   --  The rectangle is clipped if it intersects the cliping area.
   overriding
   procedure Fill (Driver : in out Driver_Type;
                   Rect   : in Rect_Type;
                   Style  : in Style_Type) is
      Info : constant Style_Info_Type := Driver.Styles (Style.Index);
      X0   : X_Type := Rect.X;
      Y0   : constant Y_Type := Rect.Y;
   begin
      while X0 < Rect.X + Rect.W loop
         if X0 >= Driver.Clip.X + Driver.Clip.W then
            return;
         end if;
         if Y0 >= Driver.Clip.Y + Driver.Clip.H then
            return;
         end if;
         if X0 >= Driver.Clip.X then
            Driver.Line.Content (X0, Y0) := (Info.Fill, Style);
            if Driver.Line.Min > X0 then
               Driver.Line.Min := X0;
            end if;
            if Driver.Line.Max < X0 then
               Driver.Line.Max := X0;
            end if;
         end if;
         X0 := X0 + 1;
      end loop;
   end Fill;

   --  Draw a line starting from (X1, Y1) to (X2, Y2) using the given style.
   --  The line is clipped if it intersects the cliping area.
   overriding
   procedure Draw (Driver : in out Driver_Type;
                   X1     : in X_Type;
                   Y1     : in Y_Type;
                   X2     : in X_Type;
                   Y2     : in Y_Type;
                   Rect   : in Rect_Type;
                   Style  : in Style_Type) is
   begin
      null;
   end Draw;

   --  Write the text starting at the top left X, Y corner and using the
   --  given style.  The text is clipped if it intersects the cliping area.
   --  An UTF-8 sequence must be converted to a Wide_Wide_String and
   --  the Put_Wide method must be used.
   overriding
   procedure Put (Driver  : in out Driver_Type;
                  X       : in X_Type;
                  Y       : in Y_Type;
                  Text    : in String;
                  Style   : in Style_Type;
                  Justify : in Justify_Type := J_DEFAULT) is
      X0  : X_Type := X;
      Pos : Natural := Text'First;
   begin
      while Pos <= Text'Last loop
         if X0 >= Driver.Clip.X + Driver.Clip.W then
            return;
         end if;
         if Y >= Driver.Clip.Y + Driver.Clip.H then
            return;
         end if;
         if X0 >= Driver.Clip.X and Y >= Driver.Clip.Y then
            Driver.Line.Content (X0, Y) := (To_WChar (Text (Pos)), Style);
            if Driver.Line.Min > X0 then
               Driver.Line.Min := X0;
            end if;
            if Driver.Line.Max < X0 then
               Driver.Line.Max := X0;
            end if;
         end if;
         Pos := Pos + 1;
         X0 := X0 + 1;
      end loop;
   end Put;

   overriding
   procedure Put_Wide (Driver  : in out Driver_Type;
                       X       : in X_Type;
                       Y       : in Y_Type;
                       Text    : in Wide_Wide_String;
                       Style   : in Style_Type;
                       Justify : in Justify_Type := J_DEFAULT) is
      X0  : X_Type := X;
      Pos : Natural := Text'First;
   begin
      while Pos <= Text'Last loop
         if X0 > Driver.Clip.X + Driver.Clip.W then
            return;
         end if;
         if Y > Driver.Clip.Y + Driver.Clip.H then
            return;
         end if;
         if X0 >= Driver.Clip.X and Y >= Driver.Clip.Y then
            Driver.Line.Content (X0, Y) := (Text (Pos), Style);
            if Driver.Line.Min > X0 then
               Driver.Line.Min := X0;
            end if;
            if Driver.Line.Max < X0 then
               Driver.Line.Max := X0;
            end if;
         end if;
         Pos := Pos + 1;
         X0 := X0 + 1;
      end loop;
   end Put_Wide;

   overriding
   procedure Put_Wide (Driver  : in out Driver_Type;
                       Box     : in Rect_Type;
                       Text    : in Wide_Wide_String;
                       Style   : in Style_Type;
                       Justify : in Justify_Type := J_DEFAULT) is
      Style_Info : constant Style_Info_Type := Driver.Styles (Style.Index);
      Format : constant Justify_Type :=
        (if Justify = J_DEFAULT then Style_Info.Justify else Justify);
   begin
      Clip (Driver, Box);
      if Format = J_LEFT or else Format = J_DEFAULT then
         Driver.Put_Wide (Box.X, Box.Y, Text, Style);
         return;
      end if;
      declare
         First : Natural := Text'First;
         Last  : Natural := Text'Last;
         Dim   : Dimension_Type;
      begin
         while First <= Last loop
            Dim := Driver.Get_Wide_Dimension (Text (First .. Last), Style);
            if Dim.W <= Box.W then
               if Format = J_RIGHT then
                  Driver.Put_Wide (Box.X + Box.W - Dim.W, Box.Y,
                                   Text (First .. Last), Style);
               else
                  Driver.Put_Wide (Box.X + (Box.W - Dim.W) / 2, Box.Y,
                                   Text (First .. Last), Style);
               end if;
               return;
            end if;

            if Format = J_RIGHT then
               First := First + 1;
            else
               First := First + 1;
               Last := Last - 1;
            end if;
         end loop;
      end;
   end Put_Wide;

   procedure Flush (Printer : in out Printer_Type'Class) is
      procedure Set_Font (Font : Font_Type; State : in AnsiAda.States);

      procedure Set_Font (Font : Font_Type; State : in AnsiAda.States) is
      begin
         case Font is
            when F_NORMAL =>
               null;
            when F_BOLD =>
               Put (AnsiAda.Style (AnsiAda.Bright, State));
            when F_ITALIC =>
               Put (AnsiAda.Style (AnsiAda.Italic, State));
         end case;
      end Set_Font;

      Driver : constant Driver_Access := Printer.Text_Driver;
      S      : Wide_Wide_String (1 .. 1);
   begin
      for Y in 1 .. Driver.Rows loop
         for I in 1 .. Driver.Line.Max loop
            declare
               Cell  : constant Cell_Type := Driver.Line.Content (I, Y);
               Info  : constant Style_Info_Type := Driver.Styles (Cell.Style.Index);
            begin
               if Info.Color /= Driver.Current.Color then
                  Driver.Current.Color := Info.Color;
                  Put (To_Foreground (Driver.Current.Color));
               end if;
               if Info.Background /= Driver.Current.Background then
                  Driver.Current.Background := Info.Background;
                  Put (To_Background (Driver.Current.Background));
               end if;
               if Info.Font /= Driver.Current.Font then
                  Set_Font (Driver.Current.Font, AnsiAda.Off);
                  Set_Font (Info.Font, AnsiAda.On);
                  Driver.Current.Font := Info.Font;
               end if;
               S (1) := Cell.Glyph;
               Put_Wide (S);
            end;
         end loop;
      end loop;
      Driver.Clear;
      if Driver.Current.Color /= PT.Colors.White then
         Put (AnsiAda.Reset);
         Driver.Current.Color := PT.Colors.White;
      end if;
      if Driver.Current.Font /= F_NORMAL then
         Set_Font (Driver.Current.Font, AnsiAda.Off);
         Driver.Current.Font := F_NORMAL;
      end if;
      New_Line;
   end Flush;

   overriding
   procedure New_Line (Driver : in out Driver_Type) is
   begin
      if Driver.Flush /= null then
         Driver.Flush (Driver.Printer.all);
      end if;
   end New_Line;

   procedure Clear (Driver : in out Driver_Type) is
   begin
      Driver.Line.Content := (others => (others => (' ', PT.Default_Style)));
      Driver.Line.Max := X_Type'First;
      Driver.Line.Min := X_Type'Last;
      Driver.Area.X := X_Type'First;
      Driver.Area.Y := Y_Type'First;
      Driver.Area.W := Driver.Cols;
      Driver.Area.H := Driver.Rows;
      Driver.Clip := Driver.Area;
   end Clear;

end PT.Drivers.Texts;
