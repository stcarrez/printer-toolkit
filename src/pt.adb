-----------------------------------------------------------------------
--  pt -- Printer Toolkit
--  Copyright (C) 2021, 2024 Stephane Carrez
--  Written by Stephane Carrez (Stephane.Carrez@gmail.com)
--  SPDX-License-Identifier: Apache-2.0
-----------------------------------------------------------------------
package body PT is

   function "&" (Left, Right : Rect_Type) return Rect_Type is
      Result : Rect_Type;
      X : constant X_Type := Min (Left.X + Left.W, Right.X + Right.W);
      Y : constant Y_Type := Min (Left.Y + Left.H, Right.Y + Right.H);
   begin
      Result.X := Max (Left.X, Right.X);
      Result.Y := Max (Left.Y, Right.Y);
      if X > Result.X then
         Result.W := X - Result.X;
      else
         Result.W := 0;
      end if;
      if Y > Result.Y then
         Result.H := Y - Result.Y;
      else
         Result.H := 0;
      end if;
      return Result;
   end "&";

   --  ------------------------------
   --  Returns true if the printer is initialized and ready to be used.
   --  ------------------------------
   function Is_Open (Printer : in Printer_Type) return Boolean is
   begin
      return Printer.Driver /= null;
   end Is_Open;

   --  ------------------------------
   --  Create a new style that can be configured, referenced and used.
   --  ------------------------------
   function Create_Style (Printer : in Printer_Type) return Style_Type is
      Style : Style_Type;
   begin
      Printer.Driver.Create_Style (Style);
      return Style;
   end Create_Style;

   function Create_Style (Printer : in Printer_Type;
                          Color   : in Color_Type) return Style_Type is
      Style : Style_Type;
   begin
      Printer.Driver.Create_Style (Style);
      Printer.Driver.Set_Color (Style, Color);
      return Style;
   end Create_Style;

   function Create_Style (Printer : in Printer_Type;
                          From    : in Style_Type) return Style_Type is
      Style : Style_Type;
   begin
      Printer.Driver.Create_Style (Style);
      Printer.Driver.Copy_Style (From, Style);
      return Style;
   end Create_Style;

   --  ------------------------------
   --  Copy the style definition from one style to another.
   --  ------------------------------
   procedure Copy_Style (Printer : in Printer_Type;
                         From    : in Style_Type;
                         To      : in Style_Type) is
   begin
      Printer.Driver.Copy_Style (From, To);
   end Copy_Style;

   --  ------------------------------
   --  Set to use the given color when drawing with this style.
   --  ------------------------------
   procedure Set_Color (Printer : in Printer_Type;
                        Style   : in Style_Type;
                        Color   : in Color_Type) is
   begin
      Printer.Driver.Set_Color (Style, Color);
   end Set_Color;

   --  ------------------------------
   --  Set the style justification.
   --  ------------------------------
   procedure Set_Justify (Printer   : in Printer_Type;
                          Style     : in Style_Type;
                          Justify   : in Justify_Type) is
   begin
      Printer.Driver.Set_Justify (Style, Justify);
   end Set_Justify;

   --  ------------------------------
   --  Set the font style.
   --  ------------------------------
   procedure Set_Font (Printer   : in Printer_Type;
                       Style     : in Style_Type;
                       Font      : in Font_Type) is
   begin
      Printer.Driver.Set_Font (Style, Font);
   end Set_Font;

   procedure Set_Position (Printer : in out Printer_Type;
                           X       : in X_Type;
                           Y       : in Y_Type) is
   begin
      Printer.Pos.X := X;
      Printer.Pos.Y := Y;
   end Set_Position;

   procedure Set_Position (Printer  : in out Printer_Type;
                           Position : in Position_Type) is
   begin
      Printer.Pos := Position;
   end Set_Position;

   procedure Set_Style (Printer : in out Printer_Type;
                        Style   : in Style_Type) is
   begin
      Printer.Style := Style;
   end Set_Style;

   --  ------------------------------
   --  Print a message at the current position and with the current
   --  style, then update the position according to the message dimension.
   --  ------------------------------
   procedure Put (Printer : in out Printer_Type;
                  Message : in String) is
   begin
      Printer.Put (Printer.Style, Message);
   end Put;

   procedure Put (Printer : in out Printer_Type;
                  Box     : in Rect_Type;
                  Style   : in Style_Type;
                  Content : in String) is
   begin
      Printer.Driver.Put_Wide (Box, To_WString (Content), Style);
   end Put;

   procedure Put_UTF8 (Printer : in out Printer_Type;
                       Message : in String) is
      Content : constant Wide_Wide_String
        := Ada.Strings.UTF_Encoding.Wide_Wide_Strings.Decode (Message);
   begin
      Printer.Put_Wide (Printer.Style, Content);
   end Put_UTF8;

   procedure Put_Wide (Printer : in out Printer_Type;
                       Message : in Wide_Wide_String) is
   begin
      Printer.Put_Wide (Printer.Style, Message);
   end Put_Wide;

   procedure Put_Wide (Printer : in out Printer_Type;
                       Box     : in Rect_Type;
                       Style   : in Style_Type;
                       Content : in Wide_Wide_String) is
   begin
      Printer.Driver.Put_Wide (Box, Content, Style);
   end Put_Wide;

   procedure Put_UString (Printer : in out Printer_Type;
                          Style   : in Style_Type;
                          Content : in UString) is
   begin
      Printer.Put (Style, To_String (Content));
   end Put_UString;

   procedure Put_UString (Printer : in out Printer_Type;
                          Box     : in Rect_Type;
                          Style   : in Style_Type;
                          Content : in UString) is
   begin
      Printer.Put (Box, Style, To_String (Content));
   end Put_UString;

   procedure Put (Printer : in out Printer_Type;
                  Style   : in Style_Type;
                  Content : in String) is
      Dim : constant Dimension_Type := Printer.Driver.Get_Dimension (Content, Style);
   begin
      Printer.Driver.Clip ((Printer.Pos.X, Printer.Pos.Y, Dim.W, 1));
      Printer.Driver.Put (Printer.Pos.X, Printer.Pos.Y, Content, Style);
      Printer.Pos.X := Printer.Pos.X + Dim.W;
   end Put;

   procedure Put_Wide (Printer : in out Printer_Type;
                       Style   : in Style_Type;
                       Content : in Wide_Wide_String) is
      Dim : constant Dimension_Type := Printer.Driver.Get_Wide_Dimension (Content, Style);
   begin
      Printer.Driver.Clip ((Printer.Pos.X, Printer.Pos.Y, Dim.W, 1));
      Printer.Driver.Put_Wide (Printer.Pos.X, Printer.Pos.Y, Content, Style);
      Printer.Pos.X := Printer.Pos.X + Dim.W;
   end Put_Wide;

   procedure New_Line (Printer : in out Printer_Type) is
   begin
      Printer.Driver.New_Line;
      Printer.Pos.X := 1;
      Printer.Pos.Y := 1;
   end New_Line;

   procedure New_Page (Printer : in out Printer_Type) is
   begin
      Printer.Driver.New_Page;
      Printer.Pos := (1, 1);
   end New_Page;

   --  ------------------------------
   --  Get the dimension of the text when it is written with the given style.
   --  ------------------------------
   function Get_Dimension (Printer : in Printer_Type;
                           Text    : in String;
                           Style   : in Style_Type) return Dimension_Type is
   begin
      return Printer.Driver.Get_Dimension (Text, Style);
   end Get_Dimension;

   function Get_UString_Dimension (Printer : in Printer_Type;
                                   Text    : in UString;
                                   Style   : in Style_Type) return Dimension_Type is
   begin
      return Printer.Driver.Get_Dimension (To_String (Text), Style);
   end Get_UString_Dimension;

   function Get_Wide_Dimension (Printer : in Printer_Type;
                                Text    : in Wide_Wide_String;
                                Style   : in Style_Type) return Dimension_Type is
   begin
      return Printer.Driver.Get_Wide_Dimension (Text, Style);
   end Get_Wide_Dimension;

end PT;
