-----------------------------------------------------------------------
--  pt-charts-barcharts -- Printing barcharts
--  Copyright (C) 2021 Stephane Carrez
--  Written by Stephane Carrez (Stephane.Carrez@gmail.com)
--  SPDX-License-Identifier: Apache-2.0
-----------------------------------------------------------------------

generic
   type Value_Type is private;
   with function "+" (Left, Right : in Value_Type) return Value_Type is <>;
   with function Percent (Width : in W_Type;
                          Value : in Value_Type;
                          Min   : in Value_Type;
                          Max   : in Value_Type) return W_Type is <>;
   with function "<=" (Left, Right : in Value_Type) return Boolean is <>;
package PT.Charts.Barcharts is

   type Label_Value_Type is record
      Value : Value_Type;
      Label : UString;
      Style : Style_Type;
   end record;

   type Label_Value_Array is array (Positive range <>) of Label_Value_Type;

   function Compute_Sum (Values : in Label_Value_Array) return Value_Type;

   --  Draw an horizontal stacked bar chart with the values in the form:
   --  A first line with the label, a second line with the bars and when
   --  the Convert function is provided a third line with the formatted values.
   --  Example:
   --   1   2    3           4                                     6
   --  +---+----+--------+-----------------------------------+--------------+
   --   132 1320 13020       89320                                 8930
   procedure Draw_Horizontal_Stacked
      (Printer : in out Printer_Type;
       Rect    : in Rect_Type;
       Values  : in Label_Value_Array;
       Min     : in Value_Type;
       Convert : access function (Value : in Value_Type) return String) with
     Pre => Printer.Is_Open and Values'Length >= 1;

   --  Draw a progress bar in the given rectangle to show the position
   --  of the value within the min/max value range.
   procedure Draw_Progress (Printer : in out PT.Printer_Type'Class;
                            Rect    : in Rect_Type;
                            Value   : in Value_Type;
                            Min     : in Value_Type;
                            Max     : in Value_Type;
                            Style1  : in Style_Type;
                            Style2  : in Style_Type) with
     Pre => Printer.Is_Open
     and then Min <= Max
     and then not (Value <= Min and Value /= Min)
     and then Value <= Max;

   --  Draw a simple bar in the given rectangle to show the position
   --  of the value within the min/max value range and return the width
   --  of that bar.
   procedure Draw_Bar (Printer : in out PT.Printer_Type'Class;
                       Rect    : in Rect_Type;
                       Value   : in Value_Type;
                       Min     : in Value_Type;
                       Max     : in Value_Type;
                       Style   : in Style_Type;
                       Width   : out W_Type) with
     Pre => Printer.Is_Open
     and then Min <= Max
     and then not (Value <= Min and Value /= Min)
     and then Value <= Max;

end PT.Charts.Barcharts;
