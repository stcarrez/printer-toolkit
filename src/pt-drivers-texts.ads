-----------------------------------------------------------------------
--  pt-drivers-texts -- Printer driver
--  Copyright (C) 2021, 2023, 2024 Stephane Carrez
--  Written by Stephane Carrez (Stephane.Carrez@gmail.com)
--  SPDX-License-Identifier: Apache-2.0
-----------------------------------------------------------------------
with Ada.Strings.UTF_Encoding.Wide_Wide_Strings;
private with AnsiAda;
private with PT.Colors;
package PT.Drivers.Texts with Preelaborate is

   F_THICK_UPPER : constant Wide_Wide_Character :=  Wide_Wide_Character'Val (16#2580#);
   F_UPPER : constant Wide_Wide_Character :=  Wide_Wide_Character'Val (16#2594#);

   --  Fill with line
   F_LOWER : constant Wide_Wide_Character :=  Wide_Wide_Character'Val (16#2581#);
   F_LOW1  : constant Wide_Wide_Character :=  Wide_Wide_Character'Val (16#2582#);
   F_LOW2  : constant Wide_Wide_Character :=  Wide_Wide_Character'Val (16#2583#);
   F_LOW3  : constant Wide_Wide_Character :=  Wide_Wide_Character'Val (16#2584#);
   F_MED1  : constant Wide_Wide_Character :=  Wide_Wide_Character'Val (16#2585#);
   F_MED2  : constant Wide_Wide_Character :=  Wide_Wide_Character'Val (16#2586#);
   F_MED3  : constant Wide_Wide_Character :=  Wide_Wide_Character'Val (16#2587#);
   F_FULL  : constant Wide_Wide_Character :=  Wide_Wide_Character'Val (16#2588#);

   F_HLINE1 : constant Wide_Wide_Character := Wide_Wide_Character'Val (16#2500#);
   F_HLINE2 : constant Wide_Wide_Character := Wide_Wide_Character'Val (16#2501#);

   F1 : constant Wide_Wide_Character :=  Wide_Wide_Character'Val (9600);
   F2 : constant Wide_Wide_Character :=  Wide_Wide_Character'Val (9600);

   --  Fill with shading
   F_LIGHT  : constant Wide_Wide_Character :=  Wide_Wide_Character'Val (16#2591#);
   F_MEDIUM : constant Wide_Wide_Character :=  Wide_Wide_Character'Val (16#2592#);
   F_STRONG : constant Wide_Wide_Character :=  Wide_Wide_Character'Val (16#2593#);

   F6 : constant Wide_Wide_Character :=  Wide_Wide_Character'Val (16#259E#);
   V5 : constant Wide_Wide_Character :=  Wide_Wide_Character'Val (16#2595#);

   --  Get the default terminal width.
   function Screen_Dimension return Dimension_Type;

   type Printer_Type is limited new PT.Printer_Type with private;

   function Create (Width     : in X_Type := 80;
                    Max_Width : in X_Type := 0) return Printer_Type;
   function Create (Dimension : in Dimension_Type;
                    Max_Width : in X_Type := 0) return Printer_Type;

   procedure Create (Printer   : in out Printer_Type;
                     Width     : in X_Type;
                     Max_Width : in X_Type := 0);

   --  Release the style when it is no longer used.
   procedure Delete_Style (Printer : in out Printer_Type;
                           Style   : in out Style_Type);

   --  Set the character fill style.
   procedure Set_Fill (Printer : in out Printer_Type;
                       Style   : in Style_Type;
                       Fill    : in Wide_Wide_Character);

   --  Set to use the given background color when drawing with this style.
   procedure Set_Background (Printer : in out Printer_Type;
                             Style   : in Style_Type;
                             Color   : in Color_Type);

   type Flush_Procedure is
     access procedure (Printer : in out Printer_Type'Class);

   --  Set the procedure which will be called to flush the printer's content.
   procedure Set_Flush (Printer : in out Printer_Type;
                        Flush   : in Flush_Procedure);

   --  Generic procedure to handle the flush of printer's content.
   generic
      with procedure Put (S : in String);
      with procedure Put_Wide (S : in WString);
      with procedure New_Line;
   procedure Flush (Printer : in out Printer_Type'Class);

   overriding
   procedure Initialize (Driver : in out Printer_Type);

   overriding
   procedure Finalize (Driver : in out Printer_Type);

   function To_String (S : in WString; Output_BOM : in Boolean := False) return String
                       renames Ada.Strings.UTF_Encoding.Wide_Wide_Strings.Encode;

private

   type Styles_Type is array (AnsiAda.Styles) of Boolean;

   type Style_Info_Type is record
      Color      : Color_Type := PT.Colors.White;
      Background : Color_Type := PT.Colors.Black;
      Styles     : Styles_Type := (others => False);
      Fill       : Wide_Wide_Character := F_UPPER;
      Justify    : PT.Justify_Type := J_LEFT;
      Font       : PT.Font_Type := F_NORMAL;
      Allocated  : Boolean := False;
   end record;

   type Style_Info_Array is array (Style_Index_Type'Range) of Style_Info_Type;

   type Cell_Type is record
      Glyph : Wide_Wide_Character;
      Style : Style_Type;
   end record;

   type Content_Array is array (X_Type range <>, Y_Type range <>) of Cell_Type;
   type Bound_Array is array (Y_Type range <>, Y_Type range <>) of X_Type;

   type Line_Type (Cols : X_Type; Rows : Y_Type) is record
      Content : Content_Array (1 .. Cols, 1 .. Rows);
      Max     : X_Type := X_Type'First;
      Min     : X_Type := X_Type'Last;
   end record;
   type Line_Access is access all Line_Type;

   type Printer_Access is access all Printer_Type'Class;

   type Driver_Type (Cols : X_Type; Rows : Y_Type) is limited new PT.Driver_Type with record
      Area    : Rect_Type;
      Clip    : Rect_Type;
      Styles  : Style_Info_Array;
      Current : Style_Info_Type;
      Flush   : Flush_Procedure;
      Printer : Printer_Access;
      Line    : Line_Type (Cols, Rows);
   end record;

   --  Get the drawing area coordinates.
   overriding
   function Get_Area (Driver : in Driver_Type) return Rect_Type;

   --  Get the current cliping area coordinates.
   overriding
   function Get_Clip (Driver : in Driver_Type) return Rect_Type;

   --  Get the dimension of the text when it is written with the given style.
   overriding
   function Get_Dimension (Driver : in Driver_Type;
                           Text   : in String;
                           Style  : in Style_Type) return Dimension_Type;
   overriding
   function Get_Wide_Dimension (Driver : in Driver_Type;
                                Text   : in Wide_Wide_String;
                                Style  : in Style_Type) return Dimension_Type;

   overriding
   function Get_Dimension (Driver   : in Driver_Type;
                           Percent  : in Percent_Type;
                           On_Width : in W_Type := 0) return Dimension_Type;

   --  Create a new style that can be configured, referenced and used.
   overriding
   procedure Create_Style (Driver : in out Driver_Type;
                           Style  : out Style_Type);

   --  Copy the style definition from one style to another.
   overriding
   procedure Copy_Style (Driver : in out Driver_Type;
                         From   : in Style_Type;
                         To     : in Style_Type);

   --  Set to use the given color when drawing with this style.
   overriding
   procedure Set_Color (Driver : in out Driver_Type;
                        Style  : in Style_Type;
                        Color  : in Color_Type);

   --  Set the style justification.
   overriding
   procedure Set_Justify (Driver   : in out Driver_Type;
                          Style    : in Style_Type;
                          Justify  : in Justify_Type);

   --  Set the font style.
   overriding
   procedure Set_Font (Driver   : in out Driver_Type;
                       Style    : in Style_Type;
                       Font     : in Font_Type);

   --  Set the current cliping area.
   overriding
   procedure Clip (Driver : in out Driver_Type;
                   Rect   : in Rect_Type);

   --  Fill the rectangle by using the given type.
   --  The rectangle is clipped if it intersects the cliping area.
   overriding
   procedure Fill (Driver : in out Driver_Type;
                   Rect   : in Rect_Type;
                   Style  : in Style_Type);

   --  Draw a line starting from (X1, Y1) to (X2, Y2) using the given style.
   --  The line is clipped if it intersects the cliping area.
   overriding
   procedure Draw (Driver : in out Driver_Type;
                   X1     : in X_Type;
                   Y1     : in Y_Type;
                   X2     : in X_Type;
                   Y2     : in Y_Type;
                   Rect   : in Rect_Type;
                   Style  : in Style_Type);

   --  Write the text starting at the top left X, Y corner and using the
   --  given style.  The text is clipped if it intersects the cliping area.
   --  An UTF-8 sequence must be converted to a Wide_Wide_String and
   --  the Put_Wide method must be used.
   overriding
   procedure Put (Driver  : in out Driver_Type;
                  X       : in X_Type;
                  Y       : in Y_Type;
                  Text    : in String;
                  Style   : in Style_Type;
                  Justify : in Justify_Type := J_DEFAULT);

   overriding
   procedure Put_Wide (Driver  : in out Driver_Type;
                       X       : in X_Type;
                       Y       : in Y_Type;
                       Text    : in Wide_Wide_String;
                       Style   : in Style_Type;
                       Justify : in Justify_Type := J_DEFAULT);

   overriding
   procedure Put_Wide (Driver  : in out Driver_Type;
                       Box     : in Rect_Type;
                       Text    : in Wide_Wide_String;
                       Style   : in Style_Type;
                       Justify : in Justify_Type := J_DEFAULT);

   overriding
   procedure New_Line (Driver : in out Driver_Type);

   overriding
   procedure New_Page (Driver : in out Driver_Type) is null;

   procedure Clear (Driver : in out Driver_Type);

   type Driver_Access is access all Driver_Type'Class;

   type Printer_Type is limited new PT.Printer_Type with record
      Text_Driver : Driver_Access;
      Area   : Rect_Type;
      Clip   : Rect_Type;
      Styles : Style_Info_Array;
      Flush  : Flush_Procedure;
   end record;

end PT.Drivers.Texts;
