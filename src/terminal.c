#include <stdio.h>
#include <unistd.h>

#if defined (__MSYS__) || defined(_WIN32) || defined(_WIN64)
# include <windows.h>
#else
# define HAVE_IOCTL
# include <sys/ioctl.h>
# include <termios.h>
#endif

#ifdef __APPLE__
# define _pt_get_cols pt_get_cols
# define _pt_get_rows pt_get_rows
#endif

int _pt_get_cols() 
{
#ifdef HAVE_IOCTL
   struct winsize size;
   
   if (ioctl(STDIN_FILENO, TIOCGWINSZ, &size) != 0) 
     {
        return -1;
     }
   
   return size.ws_col;
#else
    CONSOLE_SCREEN_BUFFER_INFO csbi;

    GetConsoleScreenBufferInfo(GetStdHandle(STD_OUTPUT_HANDLE), &csbi);
    return csbi.srWindow.Right - csbi.srWindow.Left + 1;
#endif
}

int _pt_get_rows() 
{
#ifdef HAVE_IOCTL
   struct winsize size;
   
   if (ioctl(STDIN_FILENO, TIOCGWINSZ, &size) != 0) 
     {
        return -1;
     }
   
   return size.ws_row;
#else
   CONSOLE_SCREEN_BUFFER_INFO csbi;

   GetConsoleScreenBufferInfo(GetStdHandle(STD_OUTPUT_HANDLE), &csbi);
   return csbi.srWindow.Bottom - csbi.srWindow.Top + 1;
#endif
}

   
