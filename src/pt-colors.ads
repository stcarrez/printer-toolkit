-----------------------------------------------------------------------
--  pt-colors -- Some pre-defined colors
--  Copyright (C) 2021, 2023, 2024 Stephane Carrez
--  Written by Stephane Carrez (Stephane.Carrez@gmail.com)
--  SPDX-License-Identifier: Apache-2.0
-----------------------------------------------------------------------

package PT.Colors with Preelaborate is

   type Hue is delta 0.01 range 0.0 .. 360.0;
   type Saturation is delta 0.001 range 0.0 .. 1.0;
   type Light is delta 0.001 range 0.0 .. 1.0;
   type Value is delta 0.001 range 0.0 .. 1.0;

   --  Convert HSV color to RVB.
   function To_Color (H : Hue; S : Saturation; V : Value) return Color_Type;

   --  Convert the color to the hexadecimal string value in the form: <rrggbb>
   function To_Hex (C : Color_Type) return String;

   MAX     : constant Color_Level := Color_Level'Last;
   DARK    : constant Color_Level := Color_Level'Last / 4;
   MID     : constant Color_Level := Color_Level'Last / 2;

   Black   : constant Color_Type := (Alpha => MAX, Red => 0, Green => 0, Blue => 0);

   Dark_Red     : constant Color_Type := (Alpha => MAX, Red => DARK, Green => 0, Blue => 0);
   Dark_Green   : constant Color_Type := (Alpha => MAX, Red => 0, Green => DARK, Blue => 0);
   Dark_Yellow  : constant Color_Type := (Alpha => MAX, Red => DARK, Green => DARK, Blue => 0);
   Dark_Blue    : constant Color_Type := (Alpha => MAX, Red => 0, Green => 0, Blue => DARK);
   Dark_Magenta : constant Color_Type := (Alpha => MAX, Red => DARK, Green => 0, Blue => DARK);
   Dark_Cyan    : constant Color_Type := (Alpha => MAX, Red => 0, Green => DARK, Blue => DARK);
   Dark_Grey    : constant Color_Type := (Alpha => MAX, Red => DARK, Green => DARK, Blue => DARK);

   Red     : constant Color_Type := (Alpha => MAX, Red => MID, Green => 0, Blue => 0);
   Green   : constant Color_Type := (Alpha => MAX, Red => 0, Green => MID, Blue => 0);
   Yellow  : constant Color_Type := (Alpha => MAX, Red => MID, Green => MID, Blue => 0);
   Blue    : constant Color_Type := (Alpha => MAX, Red => 0, Green => 0, Blue => MID);
   Magenta : constant Color_Type := (Alpha => MAX, Red => MID, Green => 0, Blue => MID);
   Cyan    : constant Color_Type := (Alpha => MAX, Red => 0, Green => MID, Blue => MID);
   Grey    : constant Color_Type := (Alpha => MAX, Red => MID, Green => MID, Blue => MID);

   Light_Red     : constant Color_Type := (Alpha => MAX, Red => MAX, Green => 0, Blue => 0);
   Light_Green   : constant Color_Type := (Alpha => MAX, Red => 0, Green => MAX, Blue => 0);
   Light_Yellow  : constant Color_Type := (Alpha => MAX, Red => MAX, Green => MAX, Blue => 0);
   Light_Blue    : constant Color_Type := (Alpha => MAX, Red => 0, Green => 0, Blue => MAX);
   Light_Magenta : constant Color_Type := (Alpha => MAX, Red => MAX, Green => 0, Blue => MAX);
   Light_Cyan    : constant Color_Type := (Alpha => MAX, Red => 0, Green => MAX, Blue => MAX);
   White         : constant Color_Type := (Alpha => MAX, Red => MAX, Green => MAX, Blue => MAX);

end PT.Colors;
