-----------------------------------------------------------------------
--  pt-drivers-texts-gnat_io -- Printer driver with GNAT.IO
--  Copyright (C) 2023 Stephane Carrez
--  Written by Stephane Carrez (Stephane.Carrez@gmail.com)
--  SPDX-License-Identifier: Apache-2.0
-----------------------------------------------------------------------
package body PT.Drivers.Texts.GNAT_IO is

   procedure Put_Wide (S : in WString) is
   begin
      GNAT.IO.Put (To_String (S));
   end Put_Wide;

   procedure New_Line is
   begin
      GNAT.IO.New_Line;
   end New_Line;

end PT.Drivers.Texts.GNAT_IO;
