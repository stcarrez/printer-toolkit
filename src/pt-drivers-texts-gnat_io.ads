-----------------------------------------------------------------------
--  pt-drivers-texts-gnat_io -- Printer driver with GNAT.IO
--  Copyright (C) 2023 Stephane Carrez
--  Written by Stephane Carrez (Stephane.Carrez@gmail.com)
--  SPDX-License-Identifier: Apache-2.0
-----------------------------------------------------------------------
with GNAT.IO;
package PT.Drivers.Texts.GNAT_IO with Preelaborate is

   procedure Put_Wide (S : in WString);
   procedure New_Line;

   procedure Flush is new PT.Drivers.Texts.Flush (GNAT.IO.Put, Put_Wide, New_Line);

end PT.Drivers.Texts.GNAT_IO;
