BUILD=
MAKE_ARGS=

ifeq ($(BUILD),coverage)
MAKE_ARGS=-- -XPT_BUILD=coverage
endif

ifeq ($(BUILD),debug)
MAKE_ARGS=-- -XPT_BUILD=debug
endif

build::
	alr --non-interactive build $(MAKE_ARGS)
	cd examples && alr --non-interactive build $(MAKE_ARGS)
	cd regtests && alr --non-interactive build $(MAKE_ARGS)

# Build and run the unit tests
test:	build
	./regtests/bin/pt-harness -xml pt-aunit.xml

clean:
	rm -rf obj bin lib
	rm -rf svg/obj svg/lib
	rm -rf regtests/obj regtests/bin regtests/lib regtests/results
	rm -rf examples/obj examples/bin examples/lib
